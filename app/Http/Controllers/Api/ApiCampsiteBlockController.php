<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\ApiController;
use App\Models\CampsiteBlock;
use Illuminate\Http\Request;

class ApiCampsiteBlockController extends ApiController
{
    public function index(Request $request)
    {   
        $keyword = $request->search;

        $query = CampsiteBlock::where(function ($q) use ($keyword) {
            if (!empty($keyword)) {
                $q->where('block', 'like', '%' . $keyword . '%');
            }
        });

        return $this->successResponse($this->bootstrapTableFormat($query, $request), 'ok');
    }

    public function store(Request $request)
    {
        $resp = CampsiteBlock::create([
            'block' => $request->block,            
            'campsite_id' => $request->campsite_id,
            'kapasitas' => $request->kapasitas,
        ]);

        return $this->successResponse($resp, 'ok');
    }

    public function show($id)
    {
        $resp = CampsiteBlock::where('id', $id)->firstOrFail();

        return $this->successResponse($resp, 'ok');
    }

    public function update(Request $request, $id)
    {
        $row = CampsiteBlock::where('id', $id)->firstOrFail();

        $row->update([
            'block' => $request->block,            
            'campsite_id' => $request->campsite_id,
            'kapasitas' => $request->kapasitas,
        ]);

        return $this->successResponse($row, 'ok');
    }

    public function destroy($id)
    {
        $resp = CampsiteBlock::where('id', $id)->firstOrFail()->delete();

        return $this->successResponse($resp, 'ok');
    }
}
