<?php

namespace App\Http\Controllers\Api;

use App\Helpers\ImageUpload;
use App\Http\Controllers\ApiController;
use App\Models\ItemVendors;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Models\Reservation;
use App\Models\User;
use App\Models\Customer;
use App\Models\HistoryItemService;
use App\Models\Package;
use App\Models\Payments;
use App\Models\Reservasi;
use DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;

class ApiReservasiController extends ApiController
{
    public function index()
    {
        $list = Reservation::with(['user', 'paket', 'payment'])->get();
        return $this->successResponse($list, 'Data Berhasil Ditemukan');
    }

    public function get_item()
    {
        $item = ItemVendors::where('stok', '>', 0)->get();
        return $this->successResponse($item, 'Data Berhasil Ditemukan');
    }

    public function itemInclude($id)
    {
        $include_item = Package::where('id', $id)->with('get_item.get_nama_barang')->first();
        return $this->successResponse($include_item, 'Data Berhasil Ditemukan');
    }

    public function datatablesItem($id, Request $request)
    {
        $reservasi_item = HistoryItemService::whereNotNull('id_barang')->with('barang')->where('id_reservasi', $id)->get();
        return $this->successResponse($reservasi_item, 'Data Berhasil Ditemukan');
    }

    public function generateOrderCode()
    {
        $q          = Reservation::count();
        $prefix     = 'O'; //prefix = O-16032020-00001
        $separator  = '-';
        $date       = date('m-d-Y');
        $number     = 1; //format
        $new_number = sprintf("%03s", $number);
        $code       = 'INV-' . ($new_number) . ($separator) .  $date;
        $order_code   = $code;
        if ($q > 0) {
            $last     = Reservation::orderBy('id', 'desc')->value('code_invoice');
            $last     = explode("-", $last);
            $order_code = 'INV-' . (sprintf("%03s", $last[1] + 1)) . ($separator) . $date ;
        }
        return $order_code;
    }

    public function storeReservasi(Request $request)
    {
        // return $request->all();
        $checkout =  Carbon::parse($request->checkin)->addHour($request->berapa_malam)->addHour(1);
        $findItemPaket = Package::with('get_item')->where('id', $request->id_paket)->first();
        $cekPaket = $findItemPaket->get_item->count();
        try{
            $saveUser = Customer::create([
                'nama' => $request->nama,
                'telepon' => $request->telepon,
            ]);

            $saveReservasi = Reservation::create([
                'id_customer' => $saveUser->id,
                'id_pembayaran' => '-',
                'id_voucher' => '-',
                'id_penanggung_jawab' => $request->id_penanggung_jawab,
                'checkin' => $request->checkin,
                'checkout' => $checkout->format('Y-m-d H:i:s'),
                'berapa_malam' => $request->berapa_malam,
                'note' => $request->note,
                'id_paket' => $request->id_paket,
                'pax' => $request->pax,
                'code_invoice' => $this->generateOrderCode(),
            ]);

            foreach ($request->include_id as $keys => $values) {
                // echo 'isi' . $value->id_item_pelayanan;
                if($cekPaket > 0){
                    $createPaket = HistoryItemService::create([
                        'id_barang' => $request->include_id[$keys],
                        'id_reservasi' => $saveReservasi->id,
                        'id_customer' => $saveUser->id,
                        'tgl_penggunaan' => '-',
                        'tgl_pengembalian' => '-',
                        'status' => 'Belum Diantar',
                        'durasi' => $request->berapa_malam,
                        'qty' => $request->include_qty[$keys],
                        'is_include_paket' => 1,
                    ]);
                }
            }

            $get_qty = $request->qty;
            $get_paket = $request->unit_id;
            $get_durasi = $request->durasi;

            
            foreach ($get_qty as $key => $value) {
                if(!empty($get_qty)){
                    HistoryItemService::create([
                        'id_reservasi' => $saveReservasi->id,
                        'id_customer' => $saveUser->id,
                        'status' => 'Belum Diantar',
                        'qty' => $get_qty[$key],
                        'id_barang' => $get_paket[$key],
                        'durasi' => $get_durasi[$key],
                        'tgl_penggunaan' => '-',
                        'tgl_pengembalian' => '-',
                        'is_include_paket' => 0,
                    ]);
                }
            }
            DB::commit();
            return $this->successResponse($saveReservasi, 'Data Berhasil Disimpan');
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function datatablesPayment($id)
    {
        $data = Payments::where('id_reservasi', $id)->orderBy('id', 'Desc')->get();
        return $this->successResponse($data, 'Data Berhasil Ditemukan');
    }

    public function payment($id, Request $request)
    {
        $path = Payments::getImagePathUpload();
        $filename = null;
        // return $request->image;
        if(!empty($request->image)){
            if($image = $request->image){
                $name = $image->getClientOriginalName();
                $img = Image::make($image);
                $img->stream(); // <-- Key point
                Storage::disk('local')->put('public/request_order_image/' . $name, $img);
                $filename = $name;
            }
        }else{
            $filename = null;
        }

        // return  $filename;
        // return $request->image;
        $getDataReservasi = Reservation::where('id', $id)->first();
        

        if($request->dp > $request->sisa_pembayaran){
            // validasi false
            return redirect()->back()->with('error', 'Jumlah Pembarayan Melebihi Sisa Pembayaran, Silahkan Periksa Kembali Nominal yang di Input. ');
            // return $this->errorResponse('data', 'Jumlah Pembarayan Melebihi Sisa Pembayaran, Silahkan Periksa Kembali Nominal yang di Input. ');
        }else{
            // validasi true
            if($request->dp == $request->sisa_pembayaran){
                // validasi jika lunsa
                

                $updatePayment = Payments::create([
                    'id_customer' => $getDataReservasi->id_customer,
                    'id_reservasi' => $getDataReservasi->id,
                    'total_pembayaran' => $request->total_pembayaran,
                    'dp' => $request->dp,
                    'sisa_pembayaran' => $request->sisa_pembayaran - $request->dp,
                    'desc_pembayaran' => $request->desc_pembayaran,
                    'status_payment' => 'Lunas',
                    'image' => $filename,
                ]);
                return redirect()->back()->with('success', 'Data Berhasil Disimpan');
                // return $this->successResponse($updatePayment, 'Data Berhasil Disimpan');
            }else{
                // validasi belum lunas
                $updatePayment = Payments::create([
                    'id_customer' => $getDataReservasi->id_customer,
                    'id_reservasi' => $getDataReservasi->id,
                    'total_pembayaran' => $request->total_pembayaran,
                    'dp' => $request->dp,
                    'sisa_pembayaran' => $request->sisa_pembayaran - $request->dp,
                    'desc_pembayaran' => $request->desc_pembayaran,
                    'status_payment' => 'Belum Lunas',
                    'image' => $filename,
                ]);
                // return $this->successResponse($updatePayment, 'Data Berhasil Disimpan');
                return redirect()->back()->with('success', 'Data Berhasil Disimpan');
            }
            
        }
    }

    public function addItem(Request $request)
    {
        // return $request->all();
        // echo $getProduk = ItemVendors::get();
        $getDataReservasi = Reservation::where('id', $request->id_reservasi)->first();
        $findItemPaket = Reservation::where('id', $request->id_reservasi)->first();
        $updatePayment = Payments::where('id_reservasi', $request->id_reservasi)->orderBy('id', 'desc')->first();
        try{
            $get_qty = $request->qty;
            $get_paket = $request->unit_id;
            $get_durasi = $request->durasi;
            $getProduk = ItemVendors::whereIn('id',  $get_paket)->get();
            
            if(!empty($get_qty)){
                foreach ($get_qty as $key => $value) {
                    HistoryItemService::create([
                        'id_customer' => $findItemPaket->id_customer,
                        'id_reservasi' => $findItemPaket->id,
                        'status' => 'Belum Diantar',
                        'qty' => $get_qty[$key],
                        'id_barang' => $get_paket[$key],
                        'durasi' => $get_durasi[$key],
                        'tgl_penggunaan' => '-',
                        'tgl_pengembalian' => '-',
                    ]);
                    
                }

            }
            $sum = 0;
            foreach($getProduk as $keys => $values){
                $sum += $values->harga_jual * $get_qty[$keys];
            }
            // echo $sum;
            $updatePayment->update([
                'status_payment' => 'Belum Lunas'
            ]);

            Payments::create([
                'id_customer' => $updatePayment->id_customer,
                'id_reservasi' => $request->id_reservasi,
                'total_pembayaran' => $updatePayment->total_pembayaran + $sum,
                'desc_pembayaran' => 'Tambahan',
                'sisa_pembayaran' => $sum + $updatePayment->sisa_pembayaran,
                'dp' => 0,
                'status_payment' => 'Belum Lunas'
            ]);

            
            DB::commit();
            // return $this->successResponse($findItemPaket, 'Data Berhasil Disimpan');
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function deleteItem($id)
    {
        $deleteItem = HistoryItemService::where('id', $id)->delete();
        return $this->successResponse($deleteItem, 'Data Berhasil Dihapus');
    }
}
