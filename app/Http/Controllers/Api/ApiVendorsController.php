<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\ApiController;
use Illuminate\Http\Request;
use App\Models\Vendors;
use Spatie\Permission\Models\Role;
use App\Models\User;
use Auth;

class ApiVendorsController extends ApiController
{
    public function index(Request $request)
    {
        $keyword = $request->search;

        $query = Vendors::with(['get_user', 'get_bisnis'])->where(function ($q) use ($keyword) {
            if (!empty($keyword)) {
                $q->where(function ($q2) use ($keyword) {
                    $q2->where('name', 'like', '%' . $keyword . '%');
                });
            }
        });

        return response()->json($this->bootstrapTableFormat($query, $request), 200);
    }

    public function store(Request $request)
    {
        $cek = User::where('id', $request->user_id)->first();
        $cekUser = Vendors::where('user_id',  $request->user_id)->count();
        if($cekUser == 1){
            return $this->errorResponse('msg', 'User Ini Sudah terdaftar Menjadi Vendor, Silahkan pilih User yang lain.');
        }else{
            $resp = Vendors::create([
                'user_id' => $request->user_id,
                'bussiness_id' => $request->bussiness_id,
                'vendor_name' => $request->vendor_name,
                'company_id' => $cek->company_id,
                'note' => $request->note,
            ]);
    
            $cek->update([
                'vendor_id' => $resp->id
            ]);
    
            return $this->successResponse($resp, 'ok');
        }
        
    }

    public function show($id)
    {
        $resp = Vendors::where('id', $id)->firstOrFail();

        return $this->successResponse($resp, 'ok');
    }

    public function update(Request $request, $id)
    {

        $row = Vendors::where('id', $id)->update([
            'user_id' => $request->user_id,
            'bussiness_id' => $request->bussiness_id,
            'vendor_name' => $request->vendor_name,
            'note' => $request->note,
        ]);

        return $this->successResponse($row, 'ok');
    }

    public function destroy($id)
    {
        $resp = Vendors::where('id', $id)->firstOrFail()->delete();

        return $this->successResponse($resp, 'ok');
    }
}
