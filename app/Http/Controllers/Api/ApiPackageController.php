<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\ApiController;
// use App\Http\Requests\BussinessRequest;
use Illuminate\Http\Request;
use App\Models\Package;
use App\Models\PackageItem;
use Auth;

class ApiPackageController extends ApiController
{
    public function index(Request $request)
    {
        $list = Package::get();
        return $this->successResponse($list, 'Data Berhasil Ditemukan');
    }

    

    public function store(Request $request)
    {
        // return $request->all();
        $resp = Package::create([
            'id_bisnis' => $request->id_bisnis,
            'harga' => $request->harga,
            'nama_paket' => $request->nama_paket,
            // 'id_item_pelayanan' => $request->id_item_pelayanan,
        ]);
            
        foreach ($request->id_item_pelayanan as $key => $value) {
            if(!empty($request->id_item_pelayanan)){
                PackageItem::create([
                    'id_paket' => $resp->id,
                    'id_item_pelayanan' => $request->id_item_pelayanan[$key],
                ]);
            }
        }

        return $this->successResponse($resp, 'ok');
    }

    public function show($id)
    {
        $resp = Package::where('id', $id)->firstOrFail();

        return $this->successResponse($resp, 'ok');
    }

    public function update(Request $request, $id)
    {
        // return $request->all();
        $row = Package::where('id', $id)->firstOrFail();
        $this->deletePaket($id);

        $row->update([
            'id_bisnis' => $request->id_bisnis,
            'harga' => $request->harga,
            'nama_paket' => $request->nama_paket,
            // 'id_item_pelayanan' => $request->id_item_pelayanan,
        ]);

        foreach ($request->id_item_pelayanan as $key => $value) {
            if(!empty($request->id_item_pelayanan)){
                PackageItem::create([
                    'id_paket' => $row->id,
                    'id_item_pelayanan' => $request->id_item_pelayanan[$key],
                ]);
            }
        }

        return $this->successResponse($row, 'ok');
    }

    public function deletePaket($id)
    {
        $getCart = PackageItem::where('id_paket', $id)->get();
        $delete = PackageItem::where('id_paket', $id)->get('id');
        $coundTmp = PackageItem::where('id_paket', $id)->count();
        for ($i = 0; $i < $coundTmp; $i++) {
            $deleteTmp = PackageItem::find($getCart[$i]->id)->delete();
        }
    }

    public function destroy($id)
    {
        $resp = Package::where('id', $id)->firstOrFail()->delete();

        return $this->successResponse($resp, 'ok');
    }
}
