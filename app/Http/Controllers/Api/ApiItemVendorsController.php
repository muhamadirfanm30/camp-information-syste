<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\ApiController;
use Illuminate\Http\Request;
use App\Models\ItemVendors;

class ApiItemVendorsController extends ApiController
{
    public function index(Request $request)
    {
        $keyword = $request->search;

        $query = ItemVendors::where(function ($q) use ($keyword) {
            if (!empty($keyword)) {
                $q->where(function ($q2) use ($keyword) {
                    $q2->where('nama_barang', 'like', '%' . $keyword . '%');
                });
            }
        });

        return response()->json($this->bootstrapTableFormat($query, $request), 200);
    }

    public function store(Request $request)
    {
        $resp = ItemVendors::create([
            'vendor_id' => $request->vendor_id,
            'nama_barang' => $request->nama_barang,
            'stok' => $request->stok,
            'harga_jual' => $request->harga_jual,
            'pembiayaan' => $request->pembiayaan,
        ]);

        return $this->successResponse($resp, 'ok');
    }

    public function show($id)
    {
        $resp = ItemVendors::where('id', $id)->firstOrFail();

        return $this->successResponse($resp, 'ok');
    }

    public function update(Request $request, $id)
    {

        $row = ItemVendors::where('id', $id)->update([
            'vendor_id' => $request->vendor_id,
            'nama_barang' => $request->nama_barang,
            'stok' => $request->stok,
            'harga_jual' => $request->harga_jual,
            'pembiayaan' => $request->pembiayaan,
        ]);

        return $this->successResponse($row, 'ok');
    }

    public function destroy($id)
    {
        $resp = ItemVendors::where('id', $id)->firstOrFail()->delete();

        return $this->successResponse($resp, 'ok');
    }
}
