<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\WebInfo;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Models\Role;

class WebController extends Controller
{
    public $viewData = [];

    public function __construct()
    {
        $this->setViewData($this->getCommonViewData());
    }

    public function setViewData(array $data)
    {
        foreach ($data as $key => $value) {
            $this->viewData[$key] = $value;
        }
        return $this;
    }


    public function getViewData()
    {
        return $this->viewData;
    }

    public function getSidebarMenu()
    {
        $route = request()->segment(2);
        $active_master = in_array($route, ['positions', 'bussiness', 'employes']) ? 'active' : '';
        $active_vendor = in_array($route, ['vendors', 'vendors-item']) ? 'active' : '';
        $active_item_management = in_array($route, ['category-item', 'item', 'kategory-attribute-item', 'attribute-item']) ? 'active' : '';
        $active_UTILITY = in_array($route, ['users', 'roles']) ? 'active' : '';

        $menus = [
            [
                'name' => 'DASHBOARD',
                'url' => url('admin/dashboard'),
                'icon' => 'fas fa-tachometer-alt',
                'active_class' => $route == 'dashboard' ? 'active' : '',
                'isSubMenu' => false,
                'submenu_open_class' => '',
                'submenu' => [],
                'permission_name' => ['DASHBOARD'],
            ],
            [
                'name' => 'MASTER',
                'url' => '#',
                'icon' => 'fas fa-tachometer-alt',
                'active_class' => $active_master,
                'submenu_open_class' => ($active_master != '') ? 'menu-open' : '',
                'isSubMenu' => true,
                'permission_name' => ['company.show', 'bussiness.show', 'employes.show', 'packages.show'],
                'submenu' => [
                    // [
                    //     'name' => 'positions',
                    //     'url' => url('admin/positions'),
                    //     'active_class' => $route == 'positions' ? 'active' : '',
                    // ],
                    [
                        'name' => 'Bussiness',
                        'url' => url('admin/bussiness'),
                        'active_class' => $route == 'bussiness' ? 'active' : '',
                        'permission_name' => ['bussiness.show'],
                    ],
                    [
                        'name' => 'Company',
                        'url' => url('admin/company'),
                        'active_class' => $route == 'company' ? 'active' : '',
                        'permission_name' => ['company.show'],
                    ],
                    [
                        'name' => 'Employes',
                        'url' => url('admin/employes'),
                        'active_class' => $route == 'employes' ? 'active' : '',
                        'permission_name' => ['employes.show'],
                    ],
                    [
                        'name' => 'Packages',
                        'url' => url('admin/packages'),
                        'active_class' => $route == 'packages' ? 'active' : '',
                        'permission_name' => ['packages.show'],
                    ],
                    [
                        'name' => 'Campsite',
                        'url' => url('admin/campsite'),
                        'active_class' => $route == 'campsite' ? 'active' : '',
                        'permission_name' => ['employes.show'],
                    ],
                    [
                        'name' => 'Campsite Blok',
                        'url' => url('admin/blok'),
                        'active_class' => $route == 'blok' ? 'active' : '',
                        'permission_name' => ['employes.show'],
                    ],
                    [
                        'name' => 'Site',
                        'url' => url('admin/site'),
                        'active_class' => $route == 'site' ? 'active' : '',
                        'permission_name' => ['employes.show'],
                    ],
                ]
            ],
            [
                'name' => 'VENDOR',
                'url' => '#',
                'icon' => 'fas fa-tachometer-alt',
                'active_class' => $active_vendor,
                'submenu_open_class' => ($active_vendor != '') ? 'menu-open' : '',
                'isSubMenu' => true,
                'permission_name' => ['VENDOR.show', 'VENDOR-ITEM.show'],
                'submenu' => [
                    // [
                    //     'name' => 'positions',
                    //     'url' => url('admin/positions'),
                    //     'active_class' => $route == 'positions' ? 'active' : '',
                    // ],
                    [
                        'name' => 'Vendors',
                        'url' => url('admin/vendors'),
                        'active_class' => $route == 'vendors' ? 'active' : '',
                        'permission_name' => ['VENDOR.show'],
                    ],
                    [
                        'name' => 'Vendors Items',
                        'url' => url('admin/vendors-item'),
                        'active_class' => $route == 'vendors-item' ? 'active' : '',
                        'permission_name' => ['VENDOR-ITEM.show'],
                    ],

                ]
            ],
            // [
            //     'name' => 'Item Management',
            //     'url' => '#',
            //     'icon' => 'fas fa-tachometer-alt',
            //     'active_class' => $active_item_management,
            //     'submenu_open_class' => ($active_item_management != '') ? 'menu-open' : '',
            //     'isSubMenu' => true,
            //     'submenu' => [
            //         [
            //             'name' => 'Kategory Item',
            //             'url' => url('admin/category-item'),
            //             'active_class' => $route == 'category-item' ? 'active' : '',
            //         ],
            //         [
            //             'name' => 'Items',
            //             'url' => url('admin/item'),
            //             'active_class' => $route == 'item' ? 'active' : '',
            //         ],
            //         [
            //             'name' => 'Kategory Atribut Item',
            //             'url' => url('admin/kategory-attribute-item'),
            //             'active_class' => $route == 'kategory-attribute-item' ? 'active' : '',
            //         ],
            //         [
            //             'name' => 'Atribute Items',
            //             'url' => url('admin/attribute-item'),
            //             'active_class' => $route == 'attribute-item' ? 'active' : '',
            //         ],
            //     ]
            // ],
            [
                'name' => 'UTILITY SYSTEM',
                'url' => '#',
                'icon' => 'fas fa-tachometer-alt',
                'active_class' => $active_UTILITY,
                'submenu_open_class' => ($active_UTILITY != '') ? 'menu-open' : '',
                'isSubMenu' => true,
                'permission_name' => ['user.show', 'role.show'],
                'submenu' => [
                    [
                        'name' => 'USER',
                        'url' => url('admin/users'),
                        'active_class' => $route == 'users' ? 'active' : '',
                        'permission_name' => ['user.show'],
                    ],
                    [
                        'name' => 'ROLE',
                        'url' => url('admin/roles'),
                        'active_class' => $route == 'roles' ? 'active' : '',
                        'permission_name' => ['role.show'],
                    ],
                ]
            ],
            // [
            //     'name' => 'LOGOUT',
            //     'url' => url('auth/logout'),
            //     'icon' => 'fas fa-tachometer-alt',
            //     'active_class' => $route == 'logout' ? 'active' : '',
            //     'isSubMenu' => false,
            //     'submenu_open_class' => '',
            //     'submenu' => []
            // ],

            [
                'name' => 'Manager Area',
                'url' => '#',
                'icon' => 'fas fa-tachometer-alt',
                'active_class' => $active_UTILITY,
                'submenu_open_class' => ($active_UTILITY != '') ? 'menu-open' : '',
                'isSubMenu' => true,
                'permission_name' => Role::where('name', 'manager-area')->first()->getAllpermissions()->pluck('name')->toArray(),
                'permission_name' => ['user.show', 'role.show'],
                'submenu' => [
                    [
                        'name' => 'RESERVASI',
                        'url' => url('admin/reservasi'),
                        'active_class' => $route == 'users' ? 'active' : '',
                        'permission_name' => ['RESERVASI.show'],
                    ],
                    [
                        'name' => 'INVOICE',
                        'url' => url('admin/invoice'),
                        'active_class' => $route == 'users' ? 'active' : '',
                        'permission_name' => ['INVOICE.show'],
                    ],
                    [
                        'name' => 'VENDOR',
                        'url' => url('admin/users'),
                        'active_class' => $route == 'users' ? 'active' : '',
                        'permission_name' => ['VENDOR.show'],
                    ],
                    [
                        'name' => 'PRODUK VENDOR',
                        'url' => url('admin/users'),
                        'active_class' => $route == 'users' ? 'active' : '',
                        'permission_name' => ['PRODUK-VENDOR.show'],
                    ],
                    [
                        'name' => 'INVENTORY',
                        'url' => url('admin/users'),
                        'active_class' => $route == 'users' ? 'active' : '',
                        'permission_name' => ['INVENTORY.show'],
                    ],
                    [
                        'name' => 'PAKET',
                        'url' => url('admin/users'),
                        'active_class' => $route == 'users' ? 'active' : '',
                        'permission_name' => ['PAKET.show'],
                    ],
                    [
                        'name' => 'LAPORAN A3',
                        'url' => url('admin/users'),
                        'active_class' => $route == 'users' ? 'active' : '',
                        'permission_name' => ['LAPORAN-A3.show'],
                    ],
                    [
                        'name' => 'USER',
                        'url' => url('admin/users'),
                        'active_class' => $route == 'users' ? 'active' : '',
                        'permission_name' => ['user.show'],
                    ],
                    [
                        'name' => 'ROLE',
                        'url' => url('admin/users'),
                        'active_class' => $route == 'users' ? 'active' : '',
                        'permission_name' => ['role.show'],
                    ],
                    [
                        'name' => 'LEVEL',
                        'url' => url('admin/users'),
                        'active_class' => $route == 'users' ? 'active' : '',
                        'permission_name' => ['LEVEL.show'],
                    ],
                    [
                        'name' => 'PEGAWAI',
                        'url' => url('admin/users'),
                        'active_class' => $route == 'users' ? 'active' : '',
                        'permission_name' => ['PEGAWAI.show'],
                    ],
                    [
                        'name' => 'CUSTOMER',
                        'url' => url('admin/users'),
                        'active_class' => $route == 'users' ? 'active' : '',
                        'permission_name' => ['CUSTOMER.show'],
                    ],
                    [
                        'name' => 'KATEGORI-CAMPSITE',
                        'url' => url('admin/users'),
                        'active_class' => $route == 'users' ? 'active' : '',
                        'permission_name' => ['KATEGORI-CAMPSITE.show'],
                    ],
                    [
                        'name' => 'CAMPSITE',
                        'url' => url('admin/users'),
                        'active_class' => $route == 'users' ? 'active' : '',
                        'permission_name' => ['CAMPSITE.show'],
                    ],
                    [
                        'name' => 'SEUP FEE',
                        'url' => url('admin/users'),
                        'active_class' => $route == 'users' ? 'active' : '',
                        'permission_name' => ['SEUP-FEE.show'],
                    ],
                    [
                        'name' => 'LANDING PAGE & NEWS CIS',
                        'url' => url('admin/users'),
                        'active_class' => $route == 'users' ? 'active' : '',
                        'permission_name' => ['LANDING-PAGE-AND-NEWS-CIS.show'],
                    ],
                    [
                        'name' => 'VOUCHER',
                        'url' => url('admin/users'),
                        'active_class' => $route == 'users' ? 'active' : '',
                        'permission_name' => ['VOUCHER.show'],
                    ],
                    [
                        'name' => 'PROFILE-USER',
                        'url' => url('admin/users'),
                        'active_class' => $route == 'users' ? 'active' : '',
                        'permission_name' => ['PROFILE-USER.show'],
                    ],
                ]
            ],

            [
                'name' => 'LANDING-PAGE',
                'url' => url('admin/dashboard'),
                'icon' => 'fas fa-tachometer-alt',
                'active_class' => $route == 'dashboard' ? 'active' : '',
                'isSubMenu' => false,
                'submenu_open_class' => '',
                'submenu' => [],
                'permission_name' => ['LANDING-PAGE-AND-NEWS-CIS']
            ],

            [
                'name' => 'Customer Service & Finance',
                'url' => '#',
                'icon' => 'fas fa-tachometer-alt',
                'active_class' => $active_UTILITY,
                'submenu_open_class' => ($active_UTILITY != '') ? 'menu-open' : '',
                'isSubMenu' => true,
                'permission_name' => Role::where('name', 'customer-service-finance')->first()->getAllpermissions()->pluck('name')->toArray(),
                'submenu' => [
                    [
                        'name' => 'RESERVASI',
                        'url' => url('admin/reservasi'),
                        'active_class' => $route == 'users' ? 'active' : '',
                        'permission_name' => ['RESERVASI.show'],
                    ],
                    [
                        'name' => 'INVOICE',
                        'url' => url('admin/invoice'),
                        'active_class' => $route == 'users' ? 'active' : '',
                        'permission_name' => ['INVOICE.show'],
                    ],
                    [
                        'name' => 'PROFILE-USER',
                        'url' => url('admin/users'),
                        'active_class' => $route == 'users' ? 'active' : '',
                        'permission_name' => ['PROFILE-USER.show'],
                    ],
                    [
                        'name' => 'LAPORAN HARIAN',
                        'url' => url('admin/users'),
                        'active_class' => $route == 'users' ? 'active' : '',
                        'permission_name' => ['LAPORAN-HARIAN.show'],
                    ],
                    [
                        'name' => 'LAPORAN MINGGUAN',
                        'url' => url('admin/users'),
                        'active_class' => $route == 'users' ? 'active' : '',
                        'permission_name' => ['LAPORAN-MINGGUAN.show'],
                    ],
                    [
                        'name' => 'VOUCHER',
                        'url' => url('admin/users'),
                        'active_class' => $route == 'users' ? 'active' : '',
                        'permission_name' => ['VOUCHER.show'],
                    ],
                    [
                        'name' => 'LAPORAN BULANAN',
                        'url' => url('admin/users'),
                        'active_class' => $route == 'users' ? 'active' : '',
                        'permission_name' => ['LAPORAN-BULANAN.show'],
                    ],
                ]
            ],

            [
                'name' => 'VENDOR',
                'url' => '#',
                'icon' => 'fas fa-tachometer-alt',
                'active_class' => $active_UTILITY,
                'submenu_open_class' => ($active_UTILITY != '') ? 'menu-open' : '',
                'isSubMenu' => true,
                'permission_name' => Role::where('name', 'vendor')->first()->getAllpermissions()->pluck('name')->toArray(),
                'submenu' => [
                    [
                        'name' => 'LAPORAN BULANAN',
                        'url' => url('admin/users'),
                        'active_class' => $route == 'users' ? 'active' : '',
                        'permission_name' => ['LAPORAN-BULANAN.show'],
                    ],
                    [
                        'name' => 'PRODUK VENDOR',
                        'url' => url('admin/users'),
                        'active_class' => $route == 'users' ? 'active' : '',
                        'permission_name' => ['INVENTORY.show'],
                    ],
                    [
                        'name' => 'INVENTORY',
                        'url' => url('admin/users'),
                        'active_class' => $route == 'users' ? 'active' : '',
                        'permission_name' => ['INVENTORY.show'],
                    ],
                    [
                        'name' => 'HISTORY PRODUK',
                        'url' => url('admin/users'),
                        'active_class' => $route == 'users' ? 'active' : '',
                        'permission_name' => ['HISTORY-PRODUK.show'],
                    ],
                    [
                        'name' => 'LAPORAN VENDOR',
                        'url' => url('admin/users'),
                        'active_class' => $route == 'users' ? 'active' : '',
                        'permission_name' => ['LAPORAN-VENDOR.show'],
                    ],
                ]
            ],

            [
                'name' => 'WAREHOUSE',
                'url' => '#',
                'icon' => 'fas fa-tachometer-alt',
                'active_class' => $active_UTILITY,
                'submenu_open_class' => ($active_UTILITY != '') ? 'menu-open' : '',
                'isSubMenu' => true,
                'permission_name' => Role::where('name', 'warehouse')->first()->getAllpermissions()->pluck('name')->toArray(),
                'submenu' => [
                    [
                        'name' => 'INVENTORY',
                        'url' => url('admin/users'),
                        'active_class' => $route == 'users' ? 'active' : '',
                        'permission_name' => ['INVENTORY.show'],
                    ],
                    [
                        'name' => 'HISTORY PRODUK',
                        'url' => url('admin/users'),
                        'active_class' => $route == 'users' ? 'active' : '',
                        'permission_name' => ['HISTORY-PRODUK.show'],
                    ],
                    [
                        'name' => 'PROFILE-USER',
                        'url' => url('admin/users'),
                        'active_class' => $route == 'users' ? 'active' : '',
                        'permission_name' => ['PROFILE-USER.show'],
                    ],
                ]
            ],

            [
                'name' => 'CHECKER & LTS',
                'url' => '#',
                'icon' => 'fas fa-tachometer-alt',
                'active_class' => $active_UTILITY,
                'submenu_open_class' => ($active_UTILITY != '') ? 'menu-open' : '',
                'isSubMenu' => true,
                'permission_name' => Role::where('name', 'checker')->first()->getAllpermissions()->pluck('name')->toArray(),
                'submenu' => [
                    [
                        'name' => 'HISTORY-PELAYANAN',
                        'url' => url('admin/users'),
                        'active_class' => $route == 'users' ? 'active' : '',
                        'permission_name' => ['HISTORY-PELAYANAN.show'],
                    ],
                    [
                        'name' => 'ADDITIONAL PAKET/PRODUK',
                        'url' => url('admin/users'),
                        'active_class' => $route == 'users' ? 'active' : '',
                        'permission_name' => ['ADDITIONAL-PAKET-OR-PRODUK.show'],
                    ],
                    [
                        'name' => 'PROFILE-USER',
                        'url' => url('admin/users'),
                        'active_class' => $route == 'users' ? 'active' : '',
                        'permission_name' => ['PROFILE-USER.show'],
                    ],
                ]
            ],

            [
                'name' => 'PENGUNJUNG',
                'url' => '#',
                'icon' => 'fas fa-tachometer-alt',
                'active_class' => $active_UTILITY,
                'submenu_open_class' => ($active_UTILITY != '') ? 'menu-open' : '',
                'isSubMenu' => true,
                'permission_name' => Role::where('name', 'pengunjung')->first()->getAllpermissions()->pluck('name')->toArray(),
                'submenu' => [
                    [
                        'name' => 'HISTORY-PELAYANAN',
                        'url' => url('admin/users'),
                        'active_class' => $route == 'users' ? 'active' : '',
                        'permission_name' => ['HISTORY-PELAYANAN.show'],
                    ],
                    [
                        'name' => 'ADDITIONAL PAKET/PRODUK',
                        'url' => url('admin/users'),
                        'active_class' => $route == 'users' ? 'active' : '',
                        'permission_name' => ['ADDITIONAL-PAKET-OR-PRODUK.show'],
                    ],
                    [
                        'name' => 'RESERVASI',
                        'url' => url('admin/reservasi'),
                        'active_class' => $route == 'users' ? 'active' : '',
                        'permission_name' => ['RESERVASI.show'],
                    ],
                    [
                        'name' => 'INVOICE',
                        'url' => url('admin/invoice'),
                        'active_class' => $route == 'users' ? 'active' : '',
                        'permission_name' => ['INVOICE.show'],
                    ],
                    [
                        'name' => 'PROFILE-USER',
                        'url' => url('admin/users'),
                        'active_class' => $route == 'users' ? 'active' : '',
                        'permission_name' => ['PROFILE-USER.show'],
                    ],
                    [
                        'name' => 'PEMBAYARAN',
                        'url' => url('admin/users'),
                        'active_class' => $route == 'users' ? 'active' : '',
                        'permission_name' => ['PEMBAYARAN.show'],
                    ],
                ]
            ],

            [
                'name' => 'PIC EVENT GATHERING',
                'url' => '#',
                'icon' => 'fas fa-tachometer-alt',
                'active_class' => $active_UTILITY,
                'submenu_open_class' => ($active_UTILITY != '') ? 'menu-open' : '',
                'isSubMenu' => true,
                'permission_name' => Role::where('name', 'pic-event-gathering')->first()->getAllpermissions()->pluck('name')->toArray(),
                'submenu' => [
                    [
                        'name' => 'RESERVASI GATHERING',
                        'url' => url('admin/users'),
                        'active_class' => $route == 'users' ? 'active' : '',
                        'permission_name' => ['RESERVASI-GATHERING.show'],
                    ],
                    [
                        'name' => 'VOUCHER',
                        'url' => url('admin/users'),
                        'active_class' => $route == 'users' ? 'active' : '',
                        'permission_name' => ['VOUCHER.show'],
                    ],
                ]
            ],

            [
                'name' => 'MARKETING',
                'url' => '#',
                'icon' => 'fas fa-tachometer-alt',
                'active_class' => $active_UTILITY,
                'submenu_open_class' => ($active_UTILITY != '') ? 'menu-open' : '',
                'isSubMenu' => true,
                'permission_name' => Role::where('name', 'marketing')->first()->getAllpermissions()->pluck('name')->toArray(),
                'submenu' => [
                    [
                        'name' => 'RESERVASI',
                        'url' => url('admin/reservasi'),
                        'active_class' => $route == 'users' ? 'active' : '',
                        'permission_name' => ['RESERVASI.show'],
                    ],
                    [
                        'name' => 'PROFILE-USER',
                        'url' => url('admin/users'),
                        'active_class' => $route == 'users' ? 'active' : '',
                        'permission_name' => ['PROFILE-USER.show'],
                    ],
                ]
            ],
        ];
        return $menus;
    }

    public function getCommonViewData()
    {
        return [
            'title' => 'myApp',
            'title_header' => '',
            'app_name' => 'myApp',
            'master_template' => 'admin.template.master',
            'sidebar_menu' => $this->getSidebarMenu(),
        ];
    }

    public function loadView($path, $data = [])
    {
        $this->setViewData($data);

        return view($path, $this->getViewData());
    }
}
