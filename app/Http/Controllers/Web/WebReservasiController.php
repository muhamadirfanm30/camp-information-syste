<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\WebController;
use App\Models\Package;
use App\Models\Payments;
use App\Models\Reservation;
use Illuminate\Http\Request;
use App\Models\User;

class WebReservasiController extends WebController
{
    public function index()
    {
        return $this->loadView('admin.reservasi.index', [
            'title' => 'List Reservations'
        ]);
    }
    

    public function create()
    {
        return $this->loadView('admin.reservasi.create', [
            'title' => 'Reservation Create',
            'users' => User::get(),
            'package' => Package::get(),
        ]);
    }

    public function paymentDetail($id)
    {
        $getTransaksi = Payments::where('id_reservasi', $id)->orderBy('id', 'Desc')->first();
        $reservasi =  Reservation::with([
            'user',
            'penanggung_jawab',
            'paket',
            'detail_reservasi.barang',
            'detail_reservasi' => function ($query) {
                $query->whereNotNull('id_barang');
            },
        ])
        ->find($id);
        return $this->loadView('admin.payment.detail', [
            'title' => 'Payment Detail',
            'payment_detail' => Payments::where('id_reservasi', $id)->first(),
            'reservasi' => $reservasi,
            'getTransaksi' => $getTransaksi,
            'id' => $id
        ]);
    }

    public function detail($id)
    {
        return $this->loadView('admin.reservasi.detail', [
            'title' => 'Reservation Update',
            'users' => User::get(),
            'package' => Package::get(),
            'reservasi' => Reservation::with('user')->find($id),
        ]);
    }

    public function viewInvoice($id)
    {
        $reservasi =  Reservation::with([
            'user',
            'penanggung_jawab',
            'paket',
            'detail_reservasi.barang',
            'detail_reservasi' => function ($query) {
                $query->whereNotNull('id_barang');
            },
        ])
        ->find($id);
        return $this->loadView('admin.reservasi.invoice', [
            'reservasi' => $reservasi
        ]);
    }
}
