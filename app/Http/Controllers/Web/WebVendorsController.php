<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\WebController;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Bussiness;
class WebVendorsController extends WebController
{
    public function index()
    {
        return $this->loadView('admin.vendors.index', [
            'title' => 'Vendor List',
            'bisnis' => Bussiness::all(),
            'user' => User::all()
        ]);
    }
}
