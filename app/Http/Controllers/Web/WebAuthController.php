<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\WebController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class WebAuthController extends WebController
{
    public function loginView()
    {
        return $this->loadView('authentication.login');
    }

    public function landingPage()
    {
        return $this->loadView('landingpage');
    }

    public function reservasi()
    {
        return $this->loadView('reservasi');
    }

    public function loginProsses(Request $request)
    {
        if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
            $request->session()->regenerate();
            return redirect()->intended('admin/dashboard');
        }

        return redirect()->back()->withErrors(['msg' => 'test']);
    }

    public function logout()
    {
        Auth::logout();
        // dd(Auth::check());
        return redirect('auth/login');
    }
}
