<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\WebController;

class WebAdminDashboardController extends WebController
{
    public function index()
    {
        return $this->loadView('admin.template.master');
    }
}
