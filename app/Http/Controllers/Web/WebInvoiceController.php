<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\WebController;
use Illuminate\Http\Request;
use App\Models\ReservasiItem;
use App\Models\Reservasi;

class WebInvoiceController extends WebController
{
    public function index()
    {
        return $this->loadView('admin.invoice.index', [
            'title' => 'Invoice List', 
            'reservasi' => Reservasi::with('get_paket')->get()
        ]);
    }

    public function invoice($id)
    {
        $data = Reservasi::with(['user', 'get_paket'])->where('id', $id)->first();
        // return $data->package;
        $item = ReservasiItem::where('id_reservasi', $id)->get();
        return view('admin.invoice.invoice', [
            'data' => $data,
            'item' => $item,
        ]);
    }
}
