<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\WebController;
use App\Models\Campsite;
use Illuminate\Http\Request;

class WebCampsiteBlokController extends WebController
{
    public function index()
    {
        $getCampsite = Campsite::get();
        return $this->loadView('admin.campsite.blok.index', [
            'title' => 'Campsite Block List',
            'getCampsite' => $getCampsite
        ]);
    }
}
