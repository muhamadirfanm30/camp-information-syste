<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\WebController;
use Illuminate\Http\Request;
use App\Models\Bussiness;
use App\Models\ItemVendors;
use App\Models\Package;
use App\Models\PackageItem;

class WebPackageController extends WebController
{
    public function index()
    {
        return $this->loadView('admin.package.index', [
            'title' => 'package',
            'bussiness' => Bussiness::get(),
            'vendor_item' => ItemVendors::get(),
        ]);
    }

    public function create()
    {
        return $this->loadView('admin.package.create', [
            'title' => 'package Create',
            'bussiness' => Bussiness::get(),
            'vendor_item' => ItemVendors::get(),
        ]);
    }

    public function update($id)
    {
        return $this->loadView('admin.package.update', [
            'title' => 'package Create',
            'bussiness' => Bussiness::get(),
            'vendor_item' => ItemVendors::get(),
            'paket_detail' => PackageItem::where('id_paket', $id)->first(),
            'data_edit' => Package::where('id', $id)->first(),
            'id' => $id,
        ]);
    }
}
