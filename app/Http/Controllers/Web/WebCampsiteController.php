<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\WebController;
use Illuminate\Http\Request;

class WebCampsiteController extends WebController
{
    public function index()
    {
        return $this->loadView('admin.campsite.camp.index', [
            'title' => 'Campsite List'
        ]);
    }
}
