<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\WebController;
use Illuminate\Http\Request;
use App\Models\Vendors;

class WebItemVendorsController extends WebController
{
    public function index()
    {
        return $this->loadView('admin.vendors-item.index', [
            'title' => 'Vendor Item List',
            'vendor' => Vendors::all(),
        ]);
    }
}
