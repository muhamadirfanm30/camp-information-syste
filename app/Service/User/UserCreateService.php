<?php

namespace App\Service\User;

use App\Models\User;
use Illuminate\Http\Request;

class UserCreateService
{
    public function create(array $data)
    {
        $user = User::create([
            'name' => $data['name'],
            'company_id' => $data['company_id'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);

        return $user;
    }

    public function createWithRequestObject(Request $request)
    {
        $user = User::create([
            'name' => $request->name,
            'company_id' => $request->company_id,
            'email' => $request->email,
            'password' => bcrypt($request->password),
        ]);

        return $user;
    }
}
