<?php

namespace App\Service\User;

use App\Models\User;
use Illuminate\Http\Request;

class UserUpdateService
{
    public function updateById($id, array $data)
    {
        $user = User::where('id', $id)->firstOrFail();

        $user->update([
            'name' => $data['name'],
            'company_id' => $data['company_id'],
            'email' => $data['email'],
        ]);

        return $user;
    }

    public function updateByIdWithRequestObject($id, Request $request)
    {
        $user = User::where('id', $id)->firstOrFail();

        $user->update([
            'name' => $request->name,
            'company_id' => $request->company_id,
            'email' => $request->email,
        ]);

        return $user;
    }
}
