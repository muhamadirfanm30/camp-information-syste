<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Bussiness extends Model
{
    use HasFactory;

    protected $fillable = [
        'nama_bisnis',
        // 'company_id',
        'latitude',
        'longitude',
        'alamat',
    ];
    
    // public function getComps()
    // {
    //     return $this->belongsTo(class)
    // }
}
