<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ItemVendors extends Model
{
    use HasFactory;
    protected $fillable = [
        'vendor_id',
        'nama_barang',
        'stok',
        'harga_jual',
        'pembiayaan',
    ];

    
}
