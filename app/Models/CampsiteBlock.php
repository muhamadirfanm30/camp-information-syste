<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CampsiteBlock extends Model
{
    use HasFactory;

    protected $fillable = [
        'campsite_id',
        'block',
        'kapasitas'
    ];
}
