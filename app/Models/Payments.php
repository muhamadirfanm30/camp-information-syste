<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Payments extends Model
{
    use HasFactory;
    protected $fillable = [
        'id_customer',
        'id_reservasi',
        'total_pembayaran',
        'dp',
        'sisa_pembayaran',
        'status_payment',
        'payment_metod',
        'desc_pembayaran',
        'image',
    ];

    public static function getImagePathUpload()
    {
        return 'public/upload-bukti-transfer';
    }
}
