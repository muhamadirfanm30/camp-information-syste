<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PackageItem extends Model
{
    use HasFactory;
    protected $fillable = [
        'id_paket', 
        'id_item_pelayanan'
    ];

    function get_nama_barang()
    {
        return $this->belongsTo(ItemVendors::class, 'id_item_pelayanan', 'id');
    }
}
