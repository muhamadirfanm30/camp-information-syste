<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TmpReservasiItem extends Model
{
    use HasFactory;
    protected $fillable = [
        'produk_name',
        'qty',
        'price',
    ];
}
