<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\models\Paket;

class Reservasi extends Model
{
    use HasFactory;
    protected $fillable = [
        'name',
        'checkin',
        'checkout',
        'pax',
        'paket',
        'dp',
        'code_invoice',
        'user_id',
    ];

    public function get_paket()
    {
        return $this->belongsTo(Paket::class, 'paket', 'id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
