<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HistoryItemService extends Model
{
    use HasFactory;
    protected $fillable = [
        'id_barang',
        'id_customer',
        'id_reservasi',
        'status',
        'tgl_penggunaan',
        'tgl_pengembalian',
        'qty',
        'is_include_paket',
        'durasi'
    ];

    public function barang()
    {
        return $this->belongsTo(ItemVendors::class, 'id_barang', 'id');
    }
}
