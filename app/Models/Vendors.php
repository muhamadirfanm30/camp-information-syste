<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Vendors extends Model
{
    use HasFactory;
    protected $fillable = [
        'user_id',
        'bussiness_id',
        'vendor_name',
        'company_id',
        'note',
    ];

    public function get_user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function get_bisnis()
    {
        return $this->belongsTo(Bussiness::class, 'bussiness_id', 'id');
    }
}
