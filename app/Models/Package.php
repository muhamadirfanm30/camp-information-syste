<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
    use HasFactory;
    protected $fillable = [
        'id_bisnis', 
        'harga', 
        'nama_paket',
    ];

    function get_layanan()
    {
        return $this->belongsTo(ItemVendors::class, 'id_item_pelayanan', 'id');
    }

    function get_item()
    {
        return $this->hasMany(PackageItem::class, 'id_paket');
    }
}
