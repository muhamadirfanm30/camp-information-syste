<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Reservation extends Model
{
    use HasFactory;
    protected $fillable = [
        'id_customer',
        'id_pembayaran',
        'id_voucher',
        'checkin',
        'checkout',
        'berapa_malam',
        'note',
        'id_paket',
        'code_invoice',
        'pax',
        'id_penanggung_jawab',
    ];

    public function user()
    {
        return $this->belongsTo(Customer::class, 'id_customer', 'id');
    }

    public function penanggung_jawab()
    {
        return $this->belongsTo(User::class, 'id_penanggung_jawab', 'id');
    }

    public function paket()
    {
        return $this->belongsTo(Package::class, 'id_paket', 'id');
    }

    public function payment()
    {
        return $this->belongsTo(Payments::class, 'id', 'id_reservasi');
    }

    public function detail_reservasi()
    {
        return $this->hasMany(HistoryItemService::class, 'id_reservasi');
    }
}
