<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateItemVendorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('item_vendors', function (Blueprint $table) {
            $table->id();
            // $table->id('vendor_id');
            $table->integer('vendor_id');
            $table->string('nama_barang');
            $table->string('stok');
            $table->string('harga_jual');
            $table->string('pembiayaan');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('item_vendors');
    }
}
