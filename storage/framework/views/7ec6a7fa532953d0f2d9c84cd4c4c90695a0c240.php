
<?php $__env->startSection('content'); ?>
<!-- Default box -->
<div class="card">
    <div class="card-header">
        <h3 class="card-title"><?php echo e($title); ?></h3>

        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                <i class="fas fa-times"></i>
            </button>
        </div>
    </div>
    <form id="save_data_reservasi">
        <div class="card-body">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Create Data Csutomer</h3>
                    <div class="card-tools"></div>
                </div>
                <div class="card-body">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-6">
                                <label for="">Nama User</label>
                                <input type="text" class="form-control nama" id="nama" name="nama">
                            </div>
                            <div class="col-md-6">
                                <label for="">Nomor Telepon</label>
                                <input type="number" class="form-control telepon" id="telepon" name="telepon">
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.card-footer-->
            </div><hr>
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Create Data Reservasi</h3>
                    <div class="card-tools"></div>
                </div>
                <div class="card-body">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-6" style="margin-bottom: 20px;">
                                <label for="">Paket</label>
                                <select name="id_paket" id="id_paket" class="form-control id_paket">
                                    <?php $__currentLoopData = $package; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $paket): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option value="<?php echo e($paket->id); ?>"><?php echo e($paket->nama_paket); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                            </div>
                            <div class="col-md-6" style="margin-bottom: 20px;">
                                <label for="">Berapa Malam</label>
                                <input type="number" class="form-control berapa_malam" id="berapa_malam" name="berapa_malam">
                            </div>
                            <div class="col-md-6" style="margin-bottom: 20px;">
                                <label for="">Checkin</label>
                                <input type="text" class="form-control checkin" id="checkin" name="checkin">
                            </div>
                            <div class="col-md-6" style="margin-bottom: 20px;">
                                <label for="">Berapa Orang</label>
                                <input type="number" class="form-control pax" id="pax" name="pax">
                            </div>
                            <div class="col-md-12">
                                <label for="">Note</label>
                                <textarea name="note" id="note" class="form-control note" cols="30" rows="5"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.card-footer-->
            </div>
            <hr>
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Additional Item (Optional)</h3>
                    <div class="card-tools"></div>
                </div>
                <div class="card-body">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-12" style="margin-top: 10px;" id="outlet-content"> </div>
                            <div class="col-md-12" style="margin-top: 10px;">
                                    <button type="button" class="btn btn-sm btn-primary waves-effect" id="btn-add-outlet"><i
                                            class="fa fa-plus"></i> Add Item</button>
                                    <button type="submit" class="btn btn-sm btn-success waves-effect"><i
                                        class="fa fa-plus"></i> Save Item</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.card-footer-->
            </div>
        </div>
    </form>
    <!-- /.card-body -->
    <div class="card-footer">
        <!-- Footer -->
    </div>
    
    <!-- /.card-footer-->
</div>
<!-- /.card -->
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.3/css/jquery.dataTables.css">
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.js"></script>

<script>
    HelperService.datepicker('.checkin')
$('#save_data_reservasi').submit(function(e){
        HelperService.loadingStart();
        var data = new FormData($('#media-form')[0]);
        var bodyFormData = new FormData();

        $('.nama').each(function(){ 
            bodyFormData.append('nama', $(this).val() ); 
        });

        $('.pax').each(function(){ 
            bodyFormData.append('pax', $(this).val() ); 
        });

        $('.telepon').each(function(){ 
            bodyFormData.append('telepon', $(this).val() ); 
        });

        $('.id_paket').each(function(){ 
            bodyFormData.append('id_paket', $(this).val() ); 
        });

        $('.berapa_malam').each(function(){ 
            bodyFormData.append('berapa_malam', $(this).val() ); 
        });

        $('.checkin').each(function(){ 
            bodyFormData.append('checkin', $(this).val() ); 
        });

        $('.checkout').each(function(){ 
            bodyFormData.append('checkout', $(this).val() ); 
        });

        $('.note').each(function(){ 
            bodyFormData.append('note', $(this).val() ); 
        });

        $('.unit-select').each(function(){ 
            bodyFormData.append('unit_id[]', $(this).val() ); 
        });

        $('.qty').each(function(){ 
            bodyFormData.append('qty[]', $(this).val() ); 
        });
        

        Axios.post('/admin/reservasi/store', bodyFormData, {
                headers: {
                    'Content-Type': 'multipart/form-data'
                }
            })
            .then(function(response) {
                console.log(response.data.id)
                alert('data reservasi berhasil di buat');
                HelperService.loadingStop();
                window.location.href = HelperService.url('/admin/reservasi');
                // // location.reload();
                // Helper.successNotif('Success Updated');
            })
            .catch(function(error) {
                Helper.handleErrorResponse(error)
            });

        e.preventDefault();
    })
</script>

<script>
    var DATA_UNIQ = 1;

    show();
    function show(){
        $('#outlet-content').append(outletBoxContentTemplate(DATA_UNIQ, show_delete_btn = false));
        loadUnitSelect2(DATA_UNIQ);
        DATA_UNIQ++;
    }

    $('#btn-add-outlet').click(function() {
        user_id = $('.user-select').find(':selected').val();
        $('#outlet-content').append(outletBoxContentTemplate(DATA_UNIQ));
        loadUnitSelect2(DATA_UNIQ);
        DATA_UNIQ++;
    })

    $(document).on('click', '.btn-delete-outlet', function() {
        console.log('klik')
        uniq = $(this).attr('data-uniq');
        $('.container-detail-card-' + uniq).remove();
    })

    function outletBoxContentTemplate(uniq, show_delete_btn = true) {
            btn_delete_style = 'display:none';
            if(show_delete_btn){
                btn_delete_style = '';
            }
            var template = `
            <div class="card container-detail-card-${uniq}" style="margin-bottom: 20px;">
                <input type="hidden" id="get_id_uniq" value="${uniq}">
                <div class="card-body">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Item</label>
                                    <select class="form-control unit-select unit-select-${uniq}" id="unit_id" name="unit_id" style="width:100%" data-uniq="${uniq}">
                                        <option value="">Select Item</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Qty</label>
                                    <input type="number" class="form-control qty" id="qty" name="qty">
                                </div>
                            </div>
                        </div>
                    </div>
                    <button style="${btn_delete_style}" type="button" class="btn btn-sm btn-danger waves-effect btn-delete-outlet" data-uniq="${uniq}"><i
                            class="fa fa-trash"></i> Delete Item</button>
                </div>
            </div>
        `;
            return template;
    }

    function loadUnitSelect2(uniq) {
        HelperService
            .select2Static(".unit-select-" + uniq, '/admin/reservasi/select2-item', function(item) {
                return {
                    id: item.id,
                    text: item.nama_barang,
                    data: item,
                }
            })
    }

</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make($master_template, \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\XAMPP\htdocs\web-mod\web\resources\views/admin/reservasi/create.blade.php ENDPATH**/ ?>