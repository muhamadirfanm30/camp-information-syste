

<?php $__env->startSection('content'); ?>
<!-- Default box -->
<div class="card">
    <div class="card-header">
        <h3 class="card-title"><?php echo e($title); ?></h3>

        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                <i class="fas fa-times"></i>
            </button>
        </div>
    </div>
    <div class="card-body">
        <a href="<?php echo e(url('/admin/roles/create')); ?>" class="btn btn-primary add-user-btn"><i class="fa fa-plus"></i> ADD NEW ROLE</a>
        <table id="role-table"></table>
    </div>
    <!-- /.card-body -->
    <div class="card-footer">
        Footer
    </div>
    <!-- /.card-footer-->
</div>
<!-- /.card -->
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
<script>
    $(function() {
        var $table = $('#role-table');
        var $http = new HttpService({
            bootrapTable: $table,
        });

        $table.bootstrapTable({
            sortName: 'name',
            sortOrder: 'ASC',
            theadClasses: 'table-dark',
            url: HelperService.base_url + '/api/admin/roles',
            columns: [{
                    title: 'NO',
                    sortable: true,
                    formatter: function(value, row, index) {
                        return index + 1;
                    }
                },
                {
                    title: 'NAME',
                    field: 'name',
                },
                {
                    title: 'ACTION',
                    formatter: function(value, row, index) {
                        var link = '';
                        link += '<button type="button" class="btn btn-sm btn-xs btn-success edit">';
                        link += 'Edit';
                        link += '</button>';
                        link += '<button type="button" class="btn btn-sm btn-xs btn-danger delete">';
                        link += 'Delete';
                        link += '</button>';
                        return link;
                    },
                    events: {
                        'click .edit': function(e, value, row) {
                            HelperService.redirect(`/admin/roles/${row.id}/edit`)
                        },
                        'click .delete': function(e, value, row) {
                            $http.delete(`/api/admin/roles/${row.id}/delete`)
                        }
                    }
                },
            ]
        });
    })
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make($master_template, \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\web\resources\views/admin/role/index.blade.php ENDPATH**/ ?>