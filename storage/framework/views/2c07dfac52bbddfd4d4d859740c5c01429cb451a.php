<!-- jQuery -->
<script src="<?php echo e(asset('plugins/jquery/jquery.min.js')); ?>"></script>
<!-- Bootstrap 4 -->
<script src="<?php echo e(asset('plugins/bootstrap/js/bootstrap.bundle.min.js')); ?>"></script>
<!-- AdminLTE App -->
<script src="<?php echo e(asset('dist/js/adminlte.min.js')); ?>"></script>
<!-- AdminLTE for demo purposes -->
<!-- <script src="<?php echo e(asset('dist/js/demo.js')); ?>"></script> -->

<?php echo $__env->make('admin.template.js_core', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<style>
    .bootbox .modal-title{
        display: none;
    }
</style>
<script>
    function logout(){
        HelperService.confirm(function() {
            HelperService.redirect('/auth/logout');
        })
    }
</script>
<?php /**PATH C:\xampp\htdocs\web-mod\web\resources\views/admin/template/js.blade.php ENDPATH**/ ?>