<div class="sidebar">
    <!-- Sidebar user (optional) -->
    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
            <img src="<?php echo e(asset('dist/img/user2-160x160.jpg')); ?>" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info text-center">
            <a href="#" class="d-block"><?php echo e(Auth::user()->name); ?></a>
            <a href="#" class="d-block"><?php echo e(Auth::user()->get_company->name); ?></a>
        </div>
    </div>

    <!-- SidebarSearch Form -->
    <div class="form-inline">
        <div class="input-group" data-widget="sidebar-search">
            <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
            <div class="input-group-append">
                <button class="btn btn-sidebar">
                    <i class="fas fa-search fa-fw"></i>
                </button>
            </div>
        </div>
    </div>
    <?php
        $permissions = auth()->user()->getAllPermissions();
        // dd($permissions->whereIn('name', ['user.show'])->first());
    ?>
    <!-- Sidebar Menu -->
    <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
            <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
            <?php $__currentLoopData = $sidebar_menu; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $menu): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <?php if($permissions->whereIn('name', $menu['permission_name'])->first() != null): ?>
            <li class="nav-item <?php echo e($menu['active_class']); ?> <?php echo e($menu['submenu_open_class']); ?>">
                <a href="<?php echo e($menu['url']); ?>" class="nav-link">
                    <i class="nav-icon <?php echo e($menu['icon']); ?>"></i>
                    <p><?php echo e($menu['name']); ?>  <?php if($menu['isSubMenu']): ?><i class="right fas fa-angle-left"></i><?php endif; ?></p>
                </a>
                <?php if($menu['isSubMenu']): ?>
                <ul class="nav nav-treeview">
                    <?php $__currentLoopData = $menu['submenu']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $submenu): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <?php if($permissions->whereIn('name', $submenu['permission_name'])->first() != null): ?>
                    <li class="nav-item">
                        <a href="<?php echo e($submenu['url']); ?>" class="nav-link <?php echo e($submenu['active_class']); ?>">
                            <i class="far fa-circle nav-icon"></i>
                            <p><?php echo e($submenu['name']); ?></p>
                        </a>
                    </li>
                    <?php endif; ?>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </ul>
                <?php endif; ?>
            </li>
            <?php endif; ?>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            <li class="nav-item">
                <a href="#" class="nav-link" onclick="logout()">
                    <i class="nav-icon fas fa-signout"></i>
                    <p>Logout</p>
                </a>
            </li>
        </ul>
    </nav>
    <!-- /.sidebar-menu -->
</div><?php /**PATH C:\XAMPP\htdocs\web-mod\web\resources\views/admin/template/sidebar.blade.php ENDPATH**/ ?>