

<?php $__env->startSection('content'); ?>
<!-- Default box -->
<div class="card">
    <div class="card-header">
        <h3 class="card-title"><?php echo e($title); ?></h3>

        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                <i class="fas fa-times"></i>
            </button>
        </div>
    </div>
    <div class="card-body">
        <button type="button" class="btn btn-primary add-blok-btn"><i class="fa fa-plus"></i> ADD NEW BLOCK CAMP</button>
        <table id="block-table"></table>
    </div>
    <!-- /.card-body -->
    <div class="card-footer">
        Footer
    </div>
    <!-- /.card-footer-->
</div>
<!-- /.card -->


<form id="blok-create-form">
    <div id="blok-create-modal" class="modal fade" tabindex="-1" aria-labelledby="blok-create-modalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="blok-create-modalLabel">ADD NEW BLOCK
                    </h4>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="mb-3">
                        <label>Campsite</label>
                        <select class="form-control col-12" name="campsite_id" required>
                            <option selected value="">Choose...</option>
                            <?php $__currentLoopData = $getCampsite; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $campsites): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <option value="<?php echo e($campsites->id); ?>"><?php echo e($campsites->campsite); ?></option>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </select>
                    </div>
                    <div class="mb-3">
                        <label>Block</label>
                        <input type="text" name="block" class="form-control" value="" placeholder="Block" >
                    </div>
                    <div class="mb-3">
                        <label>Kapasitas</label>
                        <input type="text" name="kapasitas" class="form-control" value="" placeholder="kapasitas" >
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light" data-bs-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-dark">Save changes</button>
                </div>
            </div>
        </div>
    </div>
</form>

<form id="block-edit-form">
    <div id="block-edit-modal" class="modal fade" tabindex="-1" aria-labelledby="block-edit-modalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="block-edit-modalLabel">EDIT BLOCK CAMP
                    </h4>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="id" />
                    <div class="mb-3">
                        <label>Campsite</label>
                        <select class="form-control col-12" name="campsite_id" required>
                            <option selected value="">Choose...</option>
                            <?php $__currentLoopData = $getCampsite; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $campsites): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <option value="<?php echo e($campsites->id); ?>"><?php echo e($campsites->campsite); ?></option>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </select>
                    </div>
                    <div class="mb-3">
                        <label>Block</label>
                        <input type="text" name="block" class="form-control" value="" placeholder="Block" >
                    </div>
                    <div class="mb-3">
                        <label>Kapasitas</label>
                        <input type="text" name="kapasitas" class="form-control" value="" placeholder="kapasitas" >
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light" data-bs-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-dark">Save changes</button>
                </div>
            </div>
        </div>
    </div>
</form>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
<script>
    $(function() {
        var $table = $('#block-table');
        var $modalCreate = $('#blok-create-modal');
        var $modalEdit = $('#block-edit-modal');

        var $formCreate = new FormService($('#blok-create-form'));
        var $formEdit = new FormService($('#block-edit-form'));

        var $http = new HttpService();

        var $httpCreate = new HttpService({
            formService: $formCreate,
            bootrapTable: $table,
            bootrapModal: $modalCreate,
        });

        var $httpEdit = new HttpService({
            formService: $formEdit,
            bootrapTable: $table,
            bootrapModal: $modalEdit,
        });

        $table.bootstrapTable({
            sortName: 'block',
            sortOrder: 'ASC',
            theadClasses: 'table-dark',
            url: HelperService.base_url + '/api/admin/block-camp',
            columns: [{
                    title: 'NO',
                    sortable: true,
                    formatter: function(value, row, index) {
                        return index + 1;
                    }
                },
                {
                    title: 'Campsite',
                    field: 'campsite_id',
                    sortable: true,
                    formatter: function(value, row, index) {
                        return row.campsite_id;
                    }
                },
                {
                    title: 'Block',
                    field: 'block',
                    sortable: true,
                    formatter: function(value, row, index) {
                        return row.block;
                    }
                },
                {
                    title: 'Kapasitas',
                    field: 'kapasitas',
                    sortable: true,
                    formatter: function(value, row, index) {
                        return row.kapasitas;
                    }
                },
                {
                    title: 'ACTION',
                    formatter: function(value, row, index) {
                        var link = '';
                        link += '<button type="button" class="btn btn-sm btn-xs btn-success edit">';
                        link += 'Edit';
                        link += '</button>';
                        link += '<button type="button" class="btn btn-sm btn-xs btn-danger delete">';
                        link += 'Delete';
                        link += '</button>';
                        return link;
                    },
                    events: {
                        'click .edit': function(e, value, row) {
                            $formEdit.emptyFormData();
                            $modalEdit.modal('show')
                            $formEdit.setFormData({
                                id: row.id,
                                block: row.block,
                                kapasitas: row.kapasitas,
                                campsite_id: row.campsite_id,
                            });
                        },
                        'click .delete': function(e, value, row) {
                            $http.delete(`/api/admin/block-camp/${row.id}/delete`)
                                .then(function(resp) {
                                    $table.bootstrapTable('refresh');
                                })
                        }
                    }
                },
            ]
        });

        $('.add-blok-btn').click(function() {
            $formCreate.emptyFormData();
            $modalCreate.modal('show')
        });

        $formCreate.onSubmit(function(data) {
            $httpCreate.post('/api/admin/block-camp', data);
        });

        $formEdit.onSubmit(function(data) {
            $httpEdit.put(`/api/admin/block-camp/${data.id}/update`, data)
        });
    })
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make($master_template, \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\XAMPP\htdocs\web\resources\views/admin/campsite/blok/index.blade.php ENDPATH**/ ?>