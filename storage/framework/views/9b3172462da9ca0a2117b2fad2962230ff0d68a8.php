<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Highland | Invoice</title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo e(asset('plugins/fontawesome-free/css/all.min.css')); ?>">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo e(asset('dist/css/adminlte.min.css')); ?>">
</head>
<body class="hold-transition sidebar-mini">
    <div class="wrapper">
        <div class="invoice p-3 mb-3">
            <!-- title row -->
            <div class="row">
                <div class="col-12">
                    <h4><img src="https://highlandcamp.robust.web.id/frontend/images/logo-highland.png" alt="" width="50px">
                    <small class="float-right">Date: <?php echo e(date('d-M-y', strtotime($data->created_at))); ?></small>
                    </h4>
                </div>
            <!-- /.col -->
            </div><hr>
            <!-- info row -->
            <div class="row invoice-info">
                <div class="col-sm-4 invoice-col">
                    From
                    <address>
                    <strong><?php echo e($data->user->name); ?></strong><br>
                    Email : <?php echo e($data->user->email); ?><br>
                    <?php echo e($data->user->address); ?>

                    </address>
                </div>
            <!-- /.col -->
                <div class="col-sm-4 invoice-col">
                    To
                    <address>
                    <strong><?php echo e($data->name); ?></strong><br>
                    Checkin : <?php echo e(date('d-M-y', strtotime($data->checkin))); ?><br>
                    checkout : <?php echo e(date('d-M-y', strtotime($data->checkout))); ?><br>
                    </address>
                </div>
            <!-- /.col -->
            <?php
                $characters = '1234567890';
            ?>
            <div class="col-sm-4 invoice-col">
                <b>Invoice #<?php echo e($data->code_invoice); ?></b><br>
                <br>
                
            </div>
            <!-- /.col -->
            </div>
            <!-- /.row -->

            <!-- Table row -->
            <div class="row">
            <div class="col-12 table-responsive">
                <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Qty / PAX</th>
                        <th>Item / paket</th>
                        <th>Price</th>
                        <th>Total</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if(!empty($item)): ?>
                        <?php $__currentLoopData = $item; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $i): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php
                            $total = $i->price * $i->qty;
                        ?>
                            <tr>
                                <td><?php echo e($i->qty); ?></td>
                                <td><?php echo e($i->produk_name); ?></td>
                                <td><?php echo e($i->price); ?></td>
                                <td>Rp. <?php echo e(number_format($total)); ?></td>
                            </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <?php
                            if(!empty($total)){
                                $total = $total;
                            }else{
                                $total = 0;
                            }
                            $total1 = $data->get_paket->harga * $data->pax;
                            $subtotal = $total1 + $total;
                            $sisa = $subtotal - $data->dp;
                        ?>
                    <?php endif; ?>
                    
                    <tr>
                        <td><?php echo e($data->pax); ?></td>
                        <td><?php echo e($data->get_paket->nama); ?></td>
                        <td><?php echo e($data->get_paket->harga); ?></td>
                        <td>Rp. <?php echo e(number_format($total1)); ?></td>
                    </tr>
                </tbody>
                <tbody>
                    <tr>
                        <td colspan="3"  class="text-right">Subtotal : </td>
                        <td>Rp. <?php echo e(number_format($subtotal)); ?> </td>
                    </tr>
                    <tr>
                        <td colspan="3"  class="text-right">Dp : </td>
                        <td>Rp. <?php echo e(number_format($data->dp)); ?> </td>
                    </tr>
                    <tr>
                        <td colspan="3"  class="text-right">Sisa Pembayaran : </td>
                        <td>Rp. <?php echo e(number_format($sisa)); ?> </td>
                    </tr>
                </tbody>
                </table>
            </div>
            <!-- /.col -->
            </div>
            <!-- /.row -->

            <div class="row">
            <!-- accepted payments column -->
            <div class="col-6">
                
            </div>
            <!-- /.col -->
            </div>
            <!-- /.row -->

            <!-- this row will not appear when printing -->
            <div class="row no-print">
            <div class="col-12">
                <button type="button" onclick="window.print();return false;" class="btn btn-success float-right"><i class="fas fa-print"></i> Print
                    Print
                </button>
            </div>
            </div>
        </div>
    </div>

    <script src="<?php echo e(asset('plugins/jquery/jquery.min.js')); ?>"></script>
    <!-- Bootstrap 4 -->
    <script src="<?php echo e(asset('plugins/bootstrap/js/bootstrap.bundle.min.js')); ?>"></script>
    <!-- AdminLTE App -->
    <script src="<?php echo e(asset('dist/js/adminlte.min.js')); ?>"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="<?php echo e(asset('dist/js/demo.js')); ?>"></script>
</body>
</html>
<?php /**PATH C:\xampp\htdocs\web-mod\web\resources\views/admin/invoice/invoice.blade.php ENDPATH**/ ?>