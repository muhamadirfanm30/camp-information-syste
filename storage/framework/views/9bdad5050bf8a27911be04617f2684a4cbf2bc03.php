
<?php $__env->startSection('content'); ?>
<!-- Default box -->
<div class="card">
    <div class="card-header">
        <h3 class="card-title"><?php echo e($title); ?></h3>

        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                <i class="fas fa-times"></i>
            </button>
        </div>
    </div>
    
    <div class="card-body">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Create Data Csutomer</h3>
                <div class="card-tools"></div>
            </div>
            <input type="hidden" class="id_reservasi" name="id_reservasi" value="<?php echo e($reservasi->id); ?>">
            <div class="card-body">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-6">
                            <label for="">Nama User</label>
                            <input type="text" class="form-control nama" id="nama" name="nama" value="<?php echo e(!empty($reservasi->user) ? $reservasi->user->nama : '-'); ?>" readonly>
                        </div>
                        <div class="col-md-6">
                            <label for="">Nomor Telepon</label>
                            <input type="number" class="form-control telepon" id="telepon" name="telepon" value="<?php echo e(!empty($reservasi->user) ? $reservasi->user->telepon : '-'); ?>" readonly>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.card-footer-->
        </div><hr>
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Create Data Reservasi</h3>
                <div class="card-tools"></div>
            </div>
            <div class="card-body">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-6">
                            <label for="">Paket</label>
                            <select name="id_paket" id="id_paket" class="form-control id_paket" readonly>
                                <?php $__currentLoopData = $package; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $paket): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <option value="<?php echo e($paket->id); ?>" <?php echo e($reservasi->id_paket == $paket->id ? 'selected' : ''); ?> disabled><?php echo e($paket->nama_paket); ?></option>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label for="">Berapa Malam</label>
                            <input type="number" class="form-control berapa_malam" id="berapa_malam" name="berapa_malam" value="<?php echo e($reservasi->berapa_malam); ?>" readonly>
                        </div>
                        <div class="col-md-6">
                            <label for="">Checkin</label>
                            <input type="date" class="form-control checkin" id="checkin" name="checkin" value="<?php echo e($reservasi->checkin); ?>" readonly>
                        </div>
                        <div class="col-md-6">
                            <label for="">Checkout</label>
                            <input type="date" class="form-control checkout" id="checkout" name="checkout" value="<?php echo e($reservasi->checkout); ?>" readonly>
                        </div>
                        
                        <div class="col-md-12">
                            <label for="">Note</label>
                            <textarea name="note" id="note" class="form-control note" cols="30" rows="5" readonly><?php echo e($reservasi->note); ?></textarea>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.card-footer-->
        </div>
        <hr>
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Additional Item (Optional)</h3>
                <div class="card-tools"></div>
            </div>
            <div class="card-body">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-striped table-bordered" id="table-item">
                                <thead>
                                    <th>Item Name</th>
                                    <th>Qty</th>
                                    <th>Status</th>
                                    <th>Tanggal Pemakaian</th>
                                    <th>Tanggal Pengembalian</th>
                                    <th>Action</th>
                                </thead>
                            </table>
                        </div>
                    </div>
                    <form id="save_data_reservasi">
                        <div class="row">
                                <div class="col-md-12" style="margin-top: 10px;" id="outlet-content"> </div>
                                <div class="col-md-12" style="margin-top: 10px;">
                                    <button type="button" class="btn btn-sm btn-primary waves-effect" id="btn-add-outlet"><i
                                            class="fa fa-plus"></i> Add Item</button>
                                    <button type="submit" class="btn btn-sm btn-success waves-effect" id="btn-save-outlet"><i
                                        class="fa fa-plus"></i> Save Item</button>
                                    <a href="<?php echo e(url('/admin/reservasi')); ?>" class="btn btn-sm btn-warning waves-effect" style="color:white"><i
                                        class="fa fa-arrow-left"></i>  kembali</a>
                                </div>
                        </div
                    </form>
                </div>
            </div>
            <!-- /.card-footer-->
        </div>
    </div>
    <!-- /.card-body -->
    <div class="card-footer">
        <!-- Footer -->
    </div>
    
    <!-- /.card-footer-->
</div>
<!-- /.card -->
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.3/css/jquery.dataTables.css">
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.js"></script>

<script>
    $(document).ready(function() {
        $('#table-item').DataTable({
            language: {
                buttons: {
                    colvis: '<i class="fa fa-list-ul"></i>'
                },
                search: '',
                searchPlaceholder: "Search...",
                processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '
            },
            oLanguage: {
                sLengthMenu: "_MENU_",
            },
            buttons: [{
                    extend: 'colvis'
                },
                {
                    text: '<i class="fa fa-refresh"></i>',
                    action: function(e, dt, node, config) {
                        dt.ajax.reload();
                    }
                }
            ],
            dom: "<'row'<'col-sm-6'Bl><'col-sm-6'>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-6'i><'col-sm-6'p>>",
            ajax: {
                url: HelperService.apiUrl('/admin/reservasi/datatables-item/'+ $('.id_reservasi').val()),
                "type": "get",
            },
            // columnDefs: [
            //     {
            //         "targets": [ 1 ],
            //         "visible": false,
            //         "searchable": false
            //     },
            // ]
            columns: [
                // {
                //     data: "DT_RowIndex",
                //     name: "DT_RowIndex",
                //     sortable: false,
                //     searchable: false,
                //     width: "10%"
                // },
                {
                    data: "id_barang",
                    name: "id_barang",
                    render: function(data, type, row) {
                        if(row.barang !== null){
                            return row.barang.nama_barang;
                        }else{
                            return 'Item Tidak Ditemukan';
                        }
                    }
                },
                {
                    data: "qty",
                    name: "qty",
                    render: function(data, type, row) {
                        return row.qty;
                    }
                },
                {
                    data: "status",
                    name: "status",
                    render: function(data, type, row) {
                        if(row.status !== null){
                            return row.status;
                        }else{
                            return '-';
                        }
                    }
                },
                
                {
                    data: "tgl_penggunaan",
                    name: "tgl_penggunaan",
                    render: function(data, type, row) {
                        if(row.tgl_penggunaan !== '-'){
                            return moment(row.tgl_penggunaan).format("DD MMMM YYYY");
                        }else{
                            return '-';
                        }
                    }
                },
                {
                    data: "tgl_pengembalian",
                    name: "tgl_pengembalian",
                    render: function(data, type, row) {
                        if(row.tgl_pengembalian !== '-'){
                            return moment(row.tgl_pengembalian).format("DD MMMM YYYY");
                        }else{
                            return '-';
                        }
                    }
                },
                {
                    data: "id",
                    name: "id",
                    render: function(data, type, row) {
                        view_link = "<button class='btn btn-danger btn-sm delete-items' data-id='" + row.id + "'><i class='fa fa-trash'></i></button>";
                        // btn_delete_item = '<button type="button" data-id="' + row.request_code + '" class="btn btn-danger btn-sm delete-btb"><i class="fa fa-trash"></i></button>';
                        return view_link;
                    }
                },
            ],
        });
    });

    $(document).on('click', '.delete-items', function() {
        var id = $(this).attr('data-id');
        $.ajax({
            url: HelperService.apiUrl('/admin/reservasi/delete-item/' + id),
            type: 'DELETE',
            success: function(result) {
                // Do something with the result
                // HelperService.onSuccess(result.data.msg);
                alert('data berhasil dihapus');
                location.reload();
                // HelperService.onSuccess(result.msg);
            }
        });
        // var id = $(this).attr('data-id');

        // HelperService.confirm(function() {
        //     HelperService.loadingStart();
        //     Axios.delete('/admin/reservasi/delete-item/' + id)
        //         .then(function(response) {
        //             //send notif
        //             HelperService.onSuccess(response.data.msg);
        //             // refresh
        //             // location.reload();
        //         })
        //         .catch(function(error) {
        //             alert('gagal');
        //             HelperService.loadingStop();
        //         });
        // })
    })
</script>

<script>
    $('#save_data_reservasi').submit(function(e){
        HelperService.loadingStart();
        var data = new FormData($('#media-form')[0]);
        var bodyFormData = new FormData();

        $('.id_reservasi').each(function(){ 
            bodyFormData.append('id_reservasi', $(this).val() ); 
        });

        $('.unit-select').each(function(){ 
            bodyFormData.append('unit_id[]', $(this).val() ); 
        });

        $('.qty').each(function(){ 
            bodyFormData.append('qty[]', $(this).val() ); 
        });
        

        Axios.post('/admin/reservasi/add-new-item', bodyFormData, {
                headers: {
                    'Content-Type': 'multipart/form-data'
                }
            })
            .then(function(response) {
                console.log(response.data.id)
                HelperService.loadingStop();
                alert('item berhasil di tambahkan');
                // window.location.href = HelperService.redirect('/admin/business-to-business-user-request-order/detail-request-order/'+ $('#sparepart_detail_order_id').val());
                location.reload();
                // HelperService.successNotif('Success Updated');
            })
            .catch(function(error) {
                // HelperService.handleErrorResponse(error)
            });

        e.preventDefault();
    });
</script>

<script>
    var DATA_UNIQ = 1;
    $('#btn-add-outlet').hide();
    $('#btn-save-outlet').hide();
    show();
    function show(){
        $('#outlet-content').append(outletBoxContentTemplate(DATA_UNIQ, show_delete_btn = false));
        loadUnitSelect2(DATA_UNIQ);
        DATA_UNIQ++;
    }

    $(document).on('change', '.unit-select', function() {
        $('#btn-add-outlet').show();
        $('#btn-save-outlet').show();
    })

    $('#btn-add-outlet').click(function() {
        user_id = $('.user-select').find(':selected').val();
        $('#outlet-content').append(outletBoxContentTemplate(DATA_UNIQ));
        loadUnitSelect2(DATA_UNIQ);
        DATA_UNIQ++;
    })

    $(document).on('click', '.btn-delete-outlet', function() {
        console.log('klik')
        uniq = $(this).attr('data-uniq');
        $('.container-detail-card-' + uniq).remove();
    })

    function outletBoxContentTemplate(uniq, show_delete_btn = true) {
            btn_delete_style = 'display:none';
            if(show_delete_btn){
                btn_delete_style = '';
            }
            var template = `
            <div class="card container-detail-card-${uniq}" style="margin-bottom: 20px;">
                <input type="hidden" id="get_id_uniq" value="${uniq}">
                <div class="card-body">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Item</label>
                                    <select class="form-control unit-select unit-select-${uniq}" id="unit_id" name="unit_id" style="width:100%" data-uniq="${uniq}">
                                        <option value="">Select Item</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Qty</label>
                                    <input type="number" class="form-control qty" id="qty" name="qty">
                                </div>
                            </div>
                        </div>
                    </div>
                    <button style="${btn_delete_style}" type="button" class="btn btn-sm btn-danger waves-effect btn-delete-outlet" data-uniq="${uniq}"><i
                            class="fa fa-trash"></i> Delete Item</button>
                </div>
            </div>
        `;
            return template;
    }

    function loadUnitSelect2(uniq) {
        HelperService
            .select2Static(".unit-select-" + uniq, '/admin/reservasi/select2-item', function(item) {
                return {
                    id: item.id,
                    text: item.nama_barang,
                    data: item,
                }
            })
    }

</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make($master_template, \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\XAMPP\htdocs\web-mod\web\resources\views/admin/reservasi/detail.blade.php ENDPATH**/ ?>