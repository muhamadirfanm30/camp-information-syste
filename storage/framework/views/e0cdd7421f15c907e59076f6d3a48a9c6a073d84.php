

<?php $__env->startSection('content'); ?>
<!-- Default box -->
<div class="card">
    <div class="card-header">
        <h3 class="card-title"><?php echo e($title); ?></h3>

        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                <i class="fas fa-times"></i>
            </button>
        </div>
    </div>
    <div class="card-body">
        <form id="form-role">
            <div class="mb-3">
                <label>ROLE NAME</label>
                <input type="text" class="form-control" name="name" required>
            </div>
            <div class="mb-3">
                <label>ROLE NAME</label>
                <br>
                <?php $__currentLoopData = $permissions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $permission): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class="form-check">
                    <input type="checkbox" class="form-check-input" name="permission_id" value="<?php echo e($permission->id); ?>" id="permission-id-<?php echo e($permission->id); ?>">
                    <label class="form-check-label" for="permission-id-<?php echo e($permission->id); ?>"><?php echo e($permission->name); ?></label>
                </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </div>
            <button type="submit" class="btn btn-primary"> <i class="fa fa-plus"></i> Save</button>
        </form>
    </div>
    <!-- /.card-body -->
    <div class="card-footer">
        Footer
    </div>
    <!-- /.card-footer-->
</div>
<!-- /.card -->
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
<script>
    $(function() {
        var $form = new FormService($('#form-role')).withArrayField(['permission_id']);
        var $http = new HttpService({
            formService: $form,
        });

        $form.onSubmit(function(data) {
            $http.post('/api/admin/roles', data).then(function(resp) {
                HelperService.redirect('/admin/roles');
            })
        })
    })
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make($master_template, \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\XAMPP\htdocs\web-mod\web\resources\views/admin/role/create.blade.php ENDPATH**/ ?>