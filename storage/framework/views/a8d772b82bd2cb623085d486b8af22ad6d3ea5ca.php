
<?php $__env->startSection('content'); ?>
<div class="card">
    <div class="card-header">
        <h3 class="card-title">Packages</h3>

        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                <i class="fas fa-times"></i>
            </button>
        </div>
    </div>
    <div class="card-body">
        <button type="button" class="btn btn-primary add-package"><i class="fa fa-plus"></i> ADD NEW PACKAGE</button>
        <table id="table"></table>
    </div>
    <!-- /.card-body -->
    <div class="card-footer">
        Footer
    </div>
    <!-- /.card-footer-->
</div>
<!-- /.card -->
<form id="package-form">
    <div id="dark-header-modal" class="modal fade" tabindex="-1" aria-labelledby="dark-header-modalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="dark-header-modalLabel">Modal Heading
                    </h4>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="id">
                    <div class="mb-3">
                        <label>ID Bisnis</label>
                        <input type="text" name="id_bisnis" class="form-control" value="" placeholder="Input ID Bisnis" required>
                    </div>
                    <div class="mb-3">
                        <label>Harga</label>
                        <input type="number" name="harga" class="form-control" value="" placeholder="Input Harga" required>
                    </div>
                    <div class="mb-3">
                        <label>Nama Paket</label>
                        <input type="text" name="nama_paket" class="form-control" value="" placeholder="Input Nama Paket" required>
                    </div>
                    <div class="mb-3">
                        <label>ID Item Pelayanan</label>
                        <input type="text" name="id_item_pelayanan" class="form-control" value="" placeholder="Input ID Item Pelayanan" required>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light" data-bs-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-dark">Save changes</button>
                </div>
            </div>
        </div>
    </div>
</form>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
<style>

</style>
<script>
    $(function() {
        var $table = $('#table');
        var $modal = $('#dark-header-modal');
        var $form = new FormService($('#package-form'));
        var $http = new HttpService({
            formService: $form,
            bootrapTable: $table,
            bootrapModal: $modal,
        });

        $table.bootstrapTable({
            sortName: 'created_at',
            sortOrder: 'ASC',
            theadClasses: 'table-dark',
            url: HelperService.base_url + '/api/admin/packages',
            responseHandler: function(res) {
                return res.data;
            },
            columns: [{
                    title: 'NO',
                    sortable: true,
                    formatter: function(value, row, index) {
                        return index + 1;
                    }
                },
                {
                    title: 'ID BISNIS',
                    field: 'id_bisnis',
                    sortable: true,
                },
                {
                    title: 'HARGA',
                    field: 'harga',
                    sortable: true,
                },
                {
                    title: 'NAMA PAKET',
                    field: 'nama_paket',
                    sortable: true,
                },
                {
                    title: 'ID ITEM PELAYANAN',
                    field: 'id_item_pelayanan',
                    sortable: true,
                },
                {
                    title: 'ACTION',
                    sortable: false,
                    formatter: function(value, row, index) {
                        var link = '';
                        link += '<button type="button" class="btn btn-sm waves-effect btn-xs btn-success edit">';
                        link += 'Edit';
                        link += '</button>';
                        link += '<button type="button" class="btn btn-sm waves-effect btn-xs btn-danger delete">';
                        link += 'Delete';
                        link += '</button>';
                        return link;
                    },
                    events: {
                        'click .edit': function(e, value, row) {
                            $form.emptyFormData();
                            $modal.modal('show')
                            $modal.find('.modal-title').text('UPDATE PACKAGE');
                            $form.setFormData(row);
                        },
                        'click .delete': function(e, value, row) {
                            HelperService.confirm(function() {
                                $http.delete(`/api/admin/packages/${row.id}/delete`, row)
                            })
                        }
                    }
                },
            ]
        });

        $('.add-package').click(function() {
            $form.emptyFormData();
            $modal.modal('show')
            $modal.find('.modal-title').text('ADD PACKAGES');
        });

        $form.onSubmit(function(data) {
            // insert
            if (data.id == '') {
                $http.post('/api/admin/packages', data)
            }

            // update
            if (data.id != '') {
                $http.put(`/api/admin/packages/${data.id}/update`, data)
            }
        })
    })
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make($master_template, \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\web-mod\web\resources\views/admin/package/index.blade.php ENDPATH**/ ?>