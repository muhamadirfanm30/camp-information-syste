

<?php $__env->startSection('content'); ?>
<!-- Default box -->
<div class="card">
    <div class="card-header">
        <h3 class="card-title"><?php echo e($title); ?></h3>

        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                <i class="fas fa-times"></i>
            </button>
        </div>
    </div>
    <div class="card-body">
        <button type="button" class="btn btn-primary add-vendor-btn"><i class="fa fa-plus"></i> ADD NEW VENDORS</button>
        <table id="vendor-table"></table>
    </div>
    <!-- /.card-body -->
    <div class="card-footer">
        Footer
    </div>
    <!-- /.card-footer-->
</div>
<!-- /.card -->


<form id="vendor-create-form">
    <div id="vendor-create-modal" class="modal fade" tabindex="-1" aria-labelledby="vendor-create-modalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="vendor-create-modalLabel">ADD NEW VENDOR
                    </h4>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="mb-3">
                        <label>Bussiness</label>
                        <select class="form-control col-12" name="bussiness_id" required>
                            <option selected value="">Choose...</option>
                            <?php $__currentLoopData = $bisnis; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <option value="<?php echo e($item->id); ?>"><?php echo e($item->nama_bisnis); ?></option>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </select>
                    </div>
                    <div class="mb-3">
                        <label>Users</label>
                        <select class="form-control col-12" name="user_id" required>
                            <option selected value="">Choose...</option>
                            <?php $__currentLoopData = $user; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $usr): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <option value="<?php echo e($usr->id); ?>"><?php echo e($usr->name); ?></option>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </select>
                    </div>
                    <div class="mb-3">
                        <label>Vendor Name</label>
                        <input type="text" name="vendor_name" class="form-control" value="" placeholder="enter name" >
                    </div>
                    <div class="mb-3">
                        <label>Note</label>
                        <textarea name="note" class="form-control" id="" cols="30" rows="3"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light" data-bs-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-dark">Save changes</button>
                </div>
            </div>
        </div>
    </div>
</form>

<form id="vendor-edit-form">
    <div id="vendor-edit-modal" class="modal fade" tabindex="-1" aria-labelledby="vendor-edit-modalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="vendor-edit-modalLabel">EDIT VENDOR
                    </h4>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="id" />
                    <div class="mb-3">
                        <label>Bussiness</label>
                        <select class="form-control col-12" name="bussiness_id" required>
                            <option selected value="">Choose...</option>
                            <?php $__currentLoopData = $bisnis; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <option value="<?php echo e($item->id); ?>"><?php echo e($item->nama_bisnis); ?></option>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </select>
                    </div>
                    <div class="mb-3">
                        <label>Users</label>
                        <select class="form-control col-12" name="user_id" required>
                            <option selected value="">Choose...</option>
                            <?php $__currentLoopData = $user; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $usr): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <option value="<?php echo e($usr->id); ?>"><?php echo e($usr->name); ?></option>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </select>
                    </div>
                    <div class="mb-3">
                        <label>Vendor Name</label>
                        <input type="text" name="vendor_name" class="form-control" value="" placeholder="enter name" >
                    </div>
                    <div class="mb-3">
                        <label>Note</label>
                        <textarea name="note" class="form-control" id="" cols="30" rows="3"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light" data-bs-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-dark">Save changes</button>
                </div>
            </div>
        </div>
    </div>
</form>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
<script>
    $(function() {
        var $table = $('#vendor-table');
        var $modalCreate = $('#vendor-create-modal');
        var $modalEdit = $('#vendor-edit-modal');

        var $formCreate = new FormService($('#vendor-create-form'));
        var $formEdit = new FormService($('#vendor-edit-form'));

        var $http = new HttpService();

        var $httpCreate = new HttpService({
            formService: $formCreate,
            bootrapTable: $table,
            bootrapModal: $modalCreate,
        });

        var $httpEdit = new HttpService({
            formService: $formEdit,
            bootrapTable: $table,
            bootrapModal: $modalEdit,
        });

        $table.bootstrapTable({
            sortName: 'vendor_name',
            sortOrder: 'ASC',
            theadClasses: 'table-dark',
            url: HelperService.base_url + '/api/admin/vendors',
            columns: [{
                    title: 'NO',
                    sortable: true,
                    formatter: function(value, row, index) {
                        return index + 1;
                    }
                },
                {
                    title: 'NAME',
                    field: 'id',
                    sortable: true,
                    formatter: function(value, row, index) {
                        return row.get_user.name;
                    }
                },
                {
                    title: 'Bussiness',
                    field: 'id',
                    sortable: true,
                    formatter: function(value, row, index) {
                        return row.get_bisnis.nama_bisnis;
                    }
                },
                {
                    title: 'Vendor Name',
                    field: 'vendor_name',
                    sortable: true,
                },
                {
                    title: 'ACTION',
                    formatter: function(value, row, index) {
                        var link = '';
                        link += '<button type="button" class="btn btn-sm btn-xs btn-success edit">';
                        link += 'Edit';
                        link += '</button>';
                        link += '<button type="button" class="btn btn-sm btn-xs btn-danger delete">';
                        link += 'Delete';
                        link += '</button>';
                        return link;
                    },
                    events: {
                        'click .edit': function(e, value, row) {
                            $formEdit.emptyFormData();
                            $modalEdit.modal('show')
                            $formEdit.setFormData({
                                id: row.id,
                                user_id: row.user_id,
                                bussiness_id: row.bussiness_id,
                                vendor_name: row.vendor_name,
                                // bussiness_id: row.bussiness_id,
                            });
                        },
                        'click .delete': function(e, value, row) {
                            $http.delete(`/api/admin/vendors/${row.id}/delete`)
                                .then(function(resp) {
                                    $table.bootstrapTable('refresh');
                                })
                        }
                    }
                },
            ]
        });

        $('.add-vendor-btn').click(function() {
            $formCreate.emptyFormData();
            $modalCreate.modal('show')
        });

        $formCreate.onSubmit(function(data) {
            $httpCreate.post('/api/admin/vendors', data);
        });

        $formEdit.onSubmit(function(data) {
            $httpEdit.put(`/api/admin/vendors/${data.id}/update`, data)
        });
    })
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make($master_template, \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\XAMPP\htdocs\web-mod\web\resources\views/admin/vendors/index.blade.php ENDPATH**/ ?>