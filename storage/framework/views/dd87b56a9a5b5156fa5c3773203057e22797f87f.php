

<?php $__env->startSection('content'); ?>
<!-- Default box -->
<div class="card">
    <div class="card-header">
        <h3 class="card-title"><?php echo e($title); ?></h3>

        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                <i class="fas fa-times"></i>
            </button>
        </div>
    </div>
    <div class="card-body">
        <form id="save_package">
            <div class="mb-3" style="margin-bottom: 20px;">
                <label>Bussiness Name</label>
                <select class="form-control col-12" name="id_bisnis" id="id_bisnis" required>
                    <option selected value="">Choose...</option>
                    <?php $__currentLoopData = $bussiness; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <option value="<?php echo e($item->id); ?>"><?php echo e($item->nama_bisnis); ?></option>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </select>
            </div>
            <div class="mb-3" style="margin-bottom: 20px;">
                <label>Nama Paket</label>
                <input type="text" name="nama_paket" id="nama_paket" class="form-control" placeholder="enter Nama Paket" >
            </div>
            <div class="mb-3" style="margin-bottom: 20px;">
                <label>Pelayanan</label>
                <div class="row">
                    <?php $__currentLoopData = $vendor_item; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $vendor): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="col-md-3">
                            <div class="custom-control custom-checkbox">
                                <input class="custom-control-input id_item_pelayanan" name="id_item_pelayanan[]"  type="checkbox" id="customCheckbox<?php echo e($vendor->id); ?>" value="<?php echo e($vendor->id); ?>">
                                <label for="customCheckbox<?php echo e($vendor->id); ?>" class="custom-control-label"><?php echo e($vendor->nama_barang); ?></label>
                            </div>
                        </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>
            </div>
            <div class="mb-3" style="margin-bottom: 20px;">
                <label>Harga</label>
                <input type="number" name="harga" id="harga" class="form-control" value="" placeholder="Enter Price" >
            </div>
            <!-- <div class="col-md-12"> -->
                <button class="btn btn-primary btn-block">Save Data</button>
            <!-- </div> -->
        </form>
    </div>
    <!-- /.card-body -->
    <div class="card-footer">
        Footer
    </div>
    <!-- /.card-footer-->
</div>
<!-- /.card -->

<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
<script>
    $('#save_package').submit(function(e){
        HelperService.loadingStart();
        var data = new FormData($('#media-form')[0]);
        var bodyFormData = new FormData();

        $('#id_bisnis').each(function(){ 
            bodyFormData.append('id_bisnis', $(this).val() ); 
        });

        $('#nama_paket').each(function(){ 
            bodyFormData.append('nama_paket', $(this).val() ); 
        });

        $('.id_item_pelayanan').each(function(){ 
            if($(this).prop("checked")){
                bodyFormData.append('id_item_pelayanan[]', $(this).val() ); 
            }
        });

        $('#harga').each(function(){ 
            bodyFormData.append('harga', $(this).val() ); 
        });

        Axios.post('/admin/packages', bodyFormData, {
                headers: {
                    'Content-Type': 'multipart/form-data'
                }
            })
            .then(function(response) {
                console.log(response.data.id)
                alert('data reservasi berhasil di buat');
                HelperService.loadingStop();
                window.location.href = HelperService.url('/admin/packages');
                // // location.reload();
                // Helper.successNotif('Success Updated');
            })
            .catch(function(error) {
                Helper.handleErrorResponse(error)
            });

        e.preventDefault();
    })
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make($master_template, \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\XAMPP\htdocs\web\resources\views/admin/package/create.blade.php ENDPATH**/ ?>