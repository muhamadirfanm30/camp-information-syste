@extends($master_template)

@section('content')
<!-- Default box -->
<div class="card">
    <div class="card-header">
        <h3 class="card-title">{{ $title }}</h3>

        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                <i class="fas fa-times"></i>
            </button>
        </div>
    </div>
    <div class="card-body">
        <form id="form-role">
            <div class="mb-3">
                <label>ROLE NAME</label>
                <input type="text" class="form-control" name="name" required>
            </div>
            <div class="mb-3">
                <label>ROLE NAME</label>
                <br>
                @foreach($permissions as $permission)
                <div class="form-check">
                    <input type="checkbox" class="form-check-input" name="permission_id" value="{{ $permission->id }}" id="permission-id-{{ $permission->id }}">
                    <label class="form-check-label" for="permission-id-{{ $permission->id }}">{{ $permission->name }}</label>
                </div>
                @endforeach
            </div>
            <button type="submit" class="btn btn-primary"> <i class="fa fa-plus"></i> Save</button>
        </form>
    </div>
    <!-- /.card-body -->
    <div class="card-footer">
        Footer
    </div>
    <!-- /.card-footer-->
</div>
<!-- /.card -->
@endsection

@section('js')
<script>
    $(function() {
        var $form = new FormService($('#form-role')).withArrayField(['permission_id']);
        var $http = new HttpService({
            formService: $form,
        });

        $form.onSubmit(function(data) {
            $http.post('/api/admin/roles', data).then(function(resp) {
                HelperService.redirect('/admin/roles');
            })
        })
    })
</script>
@endsection