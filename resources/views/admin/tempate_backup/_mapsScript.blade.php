
<script>
    var map, mapEdit = true;
        var currentLocation = getCurrentLocation();

        function inputLatlng(type){
             if(type == 'new'){
                  var lat = $('#latbox').val(),
                     lng = $('#lngbox').val(),
                     mapId = "map",
                     provinceId = "province",
                     cityId = "cities",
                     boxLat = "latbox",
                     boxLng = "lngbox",
                     radius = "radius"

             }else if(type == 'edit'){
                  var lat = $('#latboxEdit').val(),
                      lng = $('#lngboxEdit').val(),
                      mapId = "mapEdit",
                      provinceId = "provinceEdit",
                      cityId = "cityEdit",
                      boxLat = "latboxEdit",
                      boxLng = "lngboxEdit",
                      radius = "radiusEdit"
             }

             if(lat && lng != ''){
                  var  myLatLng = new google.maps.LatLng( lat, lng ),
                  myOptions = {
                       zoom: 16,
                       center: myLatLng,
                       mapTypeId: google.maps.MapTypeId.ROADMAP
                  },
                  map = new google.maps.Map( document.getElementById( mapId ), myOptions ),
                  marker = new google.maps.Marker( {position: myLatLng, map: map, draggable: true} );

                  marker.setMap( map );
                  marker.setPosition( new google.maps.LatLng( lat, lng ) );
                  map.panTo( new google.maps.LatLng( lat, lng ) );
                  autoFillMapsData(marker.getPosition(), provinceId, cityId);

                  google.maps.event.addListener(marker, 'dragend', function (event) {

                       document.getElementById(boxLat).value = this.getPosition().lat();
                       document.getElementById(boxLng).value = this.getPosition().lng();
                       autoFillMapsData(this.getPosition(), provinceId, cityId);
                  });
             }
        }

        function autoFillMapsData(latlng, provinceId, cityId){
             var typeGeocoder = new google.maps.Geocoder();
             typeGeocoder.geocode({
                  "latLng" : latlng
             }, function(data, status){
                  if (status == google.maps.GeocoderStatus.OK) {
                        // console.log(data);
                      for (var i = 0; i < data[0].address_components.length; i++) {
                         if (data[0].address_components[i].types[0] == "administrative_area_level_1") {
                              var province = data[0].address_components[i].long_name;
                              document.getElementById(provinceId).value = province;
                         }else if(data[0].address_components[i].types[0] == "administrative_area_level_2"){
                              var cities = data[0].address_components[i].long_name;
                              document.getElementById(cityId).value = cities;
                         }
                      }
                      $('#detail-address-input').val(data[0].formatted_address);
                      $('#searchMap').val(data[0].formatted_address);
                      $('#searchMapEdit').val(data[0].formatted_address);
                  }
             });
        }

        initMap(currentLocation);

        function initMap(currentLocation) {
            // Create the map.
            map = new google.maps.Map(document.getElementById('map'), {
                   zoom: 15,
                   center: {lat: currentLocation.lat, lng: currentLocation.lng},
                   mapTypeId: 'roadmap',
            });

            var input = document.getElementById('searchMap');
            var searchBox = new google.maps.places.SearchBox(input);
            map.controls[google.maps.ControlPosition.TOP_LEFT].push(input); //make the searchBox inside of google maps on the top left

            // Bias the SearchBox results towards current map's viewport.
            map.addListener('bounds_changed', function() {
                searchBox.setBounds(map.getBounds());
            });

            google.maps.event.trigger($('#map'), "resize");

            var markers = [];
            google.maps.event.addListener(map, 'click', function(event) {
                markers.forEach(function(marker) {
                        marker.setMap(null);
                    });
                markers = [];
                markers.push(   new google.maps.Marker({
                                    draggable: true,
                                    position: event.latLng,
                                    map: map,
                                    title: "Your location"
                                })
                            );
                document.getElementById("latbox").value = markers[0].getPosition().lat();
                document.getElementById("lngbox").value = markers[0].getPosition().lng();
                autoFillMapsData(markers[0].getPosition(), "province", "cities");

                google.maps.event.addListener(markers[0], 'dragend', function (event) {
                    document.getElementById("latbox").value = this.getPosition().lat();
                    document.getElementById("lngbox").value = this.getPosition().lng();

                    autoFillMapsData(this.getPosition(), "province", "cities");
                });

            });

             // more details for that place.
            searchBox.addListener('places_changed', function() {
                 var places = searchBox.getPlaces();

                if (places.length == 0) {
                    return;
                }

                // For each place, get the icon, name and location.
                var bounds = new google.maps.LatLngBounds();
                places.forEach(function(place) {
                    if (!place.geometry) {
                          return;
                    }

                    var icon = {
                        url: place.icon,
                        size: new google.maps.Size(71, 71),
                        origin: new google.maps.Point(0, 0),
                        anchor: new google.maps.Point(17, 34),
                        scaledSize: new google.maps.Size(25, 25)
                    };

                    if (place.geometry.viewport) {
                        // Only geocodes have viewport.
                        bounds.union(place.geometry.viewport);
                    } else {
                        bounds.extend(place.geometry.location);
                    }

                    // Clear out the old markers.
                    markers.forEach(function(marker) {
                        marker.setMap(null);
                    });
                    markers = [];
                    markers.push(new google.maps.Marker({
                        draggable: true,
                        position: place.geometry.location,
                        map: map,
                        title: "Your location"
                    }));

                    document.getElementById("latbox").value = markers[0].getPosition().lat();
                    document.getElementById("lngbox").value = markers[0].getPosition().lng();
                    autoFillMapsData(markers[0].getPosition(), "province", "cities");

                    google.maps.event.addListener(markers[0], 'dragend', function (event) {
                        document.getElementById("latbox").value = this.getPosition().lat();
                        document.getElementById("lngbox").value = this.getPosition().lng();

                        autoFillMapsData(this.getPosition(), "province", "cities");
                    });
                });
                map.fitBounds(bounds);
            });
        }

        function initMapEdit(lats, long, res) {
            // Create the map.
            mapEdit = new google.maps.Map(document.getElementById('mapEdit'), {
                zoom: 16,
                center: {lat: lats, lng: long},
                mapTypeId: 'roadmap'
            });

            var markers = [];
            markers.push(new google.maps.Marker({
                draggable: true,
                position: {lat: lats, lng: long},
                map: mapEdit,
                title: "Your location"
            }));

            google.maps.event.addListener(markers[0], 'dragend', function (event) {
                document.getElementById("latboxEdit").value = this.getPosition().lat();
                document.getElementById("lngboxEdit").value = this.getPosition().lng();

                autoFillMapsData(this.getPosition(), "provinceEdit", "cityEdit");
            });

            var input = document.getElementById('searchMapEdit');
            var searchBox = new google.maps.places.SearchBox(input);
            mapEdit.controls[google.maps.ControlPosition.TOP_LEFT].push(input); //make the searchBox inside of google maps on the top left

            // Bias the SearchBox results towards current map's viewport.
            mapEdit.addListener('bounds_changed', function() {
                searchBox.setBounds(mapEdit.getBounds());
            });
            google.maps.event.trigger($('#mapEdit'), "resize");

              // more details for that place.
              searchBox.addListener('places_changed', function() {
                 var places = searchBox.getPlaces();

                if (places.length == 0) {
                    return;
                }

                // For each place, get the icon, name and location.
                var bounds = new google.maps.LatLngBounds();
                places.forEach(function(place) {
                    if (!place.geometry) {
                          return;
                    }

                    var icon = {
                        url: place.icon,
                        size: new google.maps.Size(71, 71),
                        origin: new google.maps.Point(0, 0),
                        anchor: new google.maps.Point(17, 34),
                        scaledSize: new google.maps.Size(25, 25)
                    };

                    if (place.geometry.viewport) {
                        // Only geocodes have viewport.
                        bounds.union(place.geometry.viewport);
                    } else {
                        bounds.extend(place.geometry.location);
                    }

                    // Clear out the old markers.
                    markers.forEach(function(marker) {
                        marker.setMap(null);
                    });
                    markers = [];
                    markers.push(new google.maps.Marker({
                        draggable: true,
                        position: place.geometry.location,
                        map: mapEdit,
                        title: "Your location"
                    }));

                    document.getElementById("latboxEdit").value = markers[0].getPosition().lat();
                    document.getElementById("lngboxEdit").value = markers[0].getPosition().lng();
                    autoFillMapsData(markers[0].getPosition(), "provinceEdit", "cityEdit");

                    google.maps.event.addListener(markers[0], 'dragend', function (event) {
                        document.getElementById("latboxEdit").value = this.getPosition().lat();
                        document.getElementById("lngboxEdit").value = this.getPosition().lng();

                        autoFillMapsData(this.getPosition(), "provinceEdit", "cityEdit");
                    });
                });
                mapEdit.fitBounds(bounds);
            });

            google.maps.event.addListener(mapEdit, 'click', function(event) {
                markers.forEach(function(marker) {
                        marker.setMap(null);
                    });
                markers = [];
                markers.push(   new google.maps.Marker({
                                    draggable: true,
                                    position: event.latLng,
                                    map: mapEdit,
                                    title: "Your location"
                                })
                            );
                document.getElementById("latboxEdit").value = markers[0].getPosition().lat();
                document.getElementById("lngboxEdit").value = markers[0].getPosition().lng();
                autoFillMapsData(markers[0].getPosition(), "provinceEdit", "cityEdit");

                google.maps.event.addListener(markers[0], 'dragend', function (event) {
                    document.getElementById("latboxEdit").value = this.getPosition().lat();
                    document.getElementById("lngboxEdit").value = this.getPosition().lng();

                    autoFillMapsData(this.getPosition(), "provinceEdit", "cityEdit");
                });

            });
        }

        function getCurrentLocation() {

             var pos = {
               lat: -6.175416873132932,
               lng: 106.82717193442386
             };

             return pos;
         }
</script>

