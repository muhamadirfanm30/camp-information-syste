@extends($master_template)
@section('content')
<!-- Default box -->
<div class="card">
    <div class="card-header">
        <h3 class="card-title">{{$title}}</h3>

        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                <i class="fas fa-times"></i>
            </button>
        </div>
    </div>
    <form id="save_data_reservasi">
        <div class="card-body">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Create Data Csutomer</h3>
                    <div class="card-tools"></div>
                </div>
                <div class="card-body">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-6">
                                <label for="">Nama User</label>
                                <input type="text" class="form-control nama" id="nama" name="nama">
                            </div>
                            <div class="col-md-6">
                                <label for="">Nomor Telepon</label>
                                <input type="number" class="form-control telepon" id="telepon" name="telepon">
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.card-footer-->
            </div><hr>
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Create Data Reservasi</h3>
                    <div class="card-tools"></div>
                </div>
                <div class="card-body">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-6" style="margin-bottom: 20px;">
                                <label for="">Paket</label>
                                <select name="id_paket" id="id_paket" class="form-control id_paket">
                                <option value="" selected>Pilih Paket</option>
                                    @foreach($package as $paket)
                                        <option value="{{ $paket->id }}">{{ $paket->nama_paket }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-6" style="margin-bottom: 20px;">
                                <label for="">Berapa Malam</label>
                                <input type="number" class="form-control berapa_malam" id="berapa_malam" name="berapa_malam">
                            </div>
                            <div class="col-md-6" style="margin-bottom: 20px;">
                                <label for="">Checkin</label>
                                <input type="text" class="form-control checkin" id="checkin" name="checkin">
                            </div>
                            <div class="col-md-6" style="margin-bottom: 20px;">
                                <label for="">Berapa Orang</label>
                                <input type="number" class="form-control pax" id="pax" name="pax">
                            </div>
                            <div class="col-md-12" style="margin-bottom: 20px;">
                                <label for="">Penanggung jawab</label>
                                <select name="id_penanggung_jawab" id="id_penanggung_jawab" class="form-control id_penanggung_jawab">
                                    @foreach($users as $usr)
                                        <option value="{{ $usr->id }}">{{ $usr->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-12">
                                <label for="">Note</label>
                                <textarea name="note" id="note" class="form-control note" cols="30" rows="5"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.card-footer-->
            </div>
            <hr>
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Additional Item (Optional)</h3>
                    <div class="card-tools"></div>
                </div>
                <div class="card-body">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-12" style="margin-top: 10px;" id="outlet-content"> </div>
                            <div class="col-md-12" style="margin-top: 10px;">
                                    <button type="button" class="btn btn-sm btn-primary waves-effect" id="btn-add-outlet"><i
                                            class="fa fa-plus"></i> Add Item</button>
                                    <button type="submit" class="btn btn-sm btn-success waves-effect"><i
                                        class="fa fa-plus"></i> Save Item</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.card-footer-->
            </div>
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Item Include In Package</h3>
                    <div class="card-tools"></div>
                </div>
                <div class="card-body" id="additional-item">

                </div>
                <!-- /.card-footer-->
            </div>
        </div>
        <button type="submit">save</button>
    </form>
    <!-- /.card-body -->
    <div class="card-footer">
        <!-- Footer -->
    </div>
    
    <!-- /.card-footer-->
</div>
<!-- /.card -->
@endsection

@section('js')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.3/css/jquery.dataTables.css">
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.js"></script>

<script>
    HelperService.datepicker('.checkin')
    // $('#save_data_reservasi-').submit(function(e){
    //     HelperService.loadingStart();
    //     var data = new FormData($('#media-form')[0]);
    //     var bodyFormData = new FormData();

    //     $('.nama').each(function(){ 
    //         bodyFormData.append('nama', $(this).val() ); 
    //     });

    //     $('.id_penanggung_jawab').each(function(){ 
    //         bodyFormData.append('id_penanggung_jawab', $(this).val() ); 
    //     });

    //     $('.pax').each(function(){ 
    //         bodyFormData.append('pax', $(this).val() ); 
    //     });

    //     $('.telepon').each(function(){ 
    //         bodyFormData.append('telepon', $(this).val() ); 
    //     });

    //     $('.id_paket').each(function(){ 
    //         bodyFormData.append('id_paket', $(this).val() ); 
    //     });

    //     $('.berapa_malam').each(function(){ 
    //         bodyFormData.append('berapa_malam', $(this).val() ); 
    //     });

    //     $('.checkin').each(function(){ 
    //         bodyFormData.append('checkin', $(this).val() ); 
    //     });

    //     $('.checkout').each(function(){ 
    //         bodyFormData.append('checkout', $(this).val() ); 
    //     });

    //     $('.note').each(function(){ 
    //         bodyFormData.append('note', $(this).val() ); 
    //     });

    //     $('.unit-select').each(function(){ 
    //         bodyFormData.append('unit_id[]', $(this).val() ); 
    //     });

    //     $('.qty').each(function(){ 
    //         bodyFormData.append('qty[]', $(this).val() ); 
    //     });

    //     $('.durasi').each(function(){ 
    //         bodyFormData.append('durasi[]', $(this).val() ); 
    //     });

    //     // form untuk include paket

    //     $('.include_item').each(function(){ 
    //         bodyFormData.append('include_item[]', $(this).val() ); 
    //     });

    //     $('.include_qty').each(function(){ 
    //         bodyFormData.append('include_qty[]', $(this).val() ); 
    //     });
        

    //     Axios.post('/admin/reservasi/store', bodyFormData, {
    //             headers: {
    //                 'Content-Type': 'multipart/form-data'
    //             }
    //         })
    //         .then(function(response) {
    //             console.log(response.data.id)
    //             alert('data reservasi berhasil di buat');
    //             HelperService.loadingStop();
    //             // window.location.href = HelperService.url('/admin/reservasi');
    //             // // location.reload();
    //             // Helper.successNotif('Success Updated');
    //         })
    //         .catch(function(error) {
    //             Helper.handleErrorResponse(error)
    //         });

    //     e.preventDefault();
    // })
</script>

<script>
    var $http = new HttpService();
    var $form = new FormService($('#save_data_reservasi')).withArrayField(['include_item', 'include_qty', 'include_id', 'unit_id', 'qty', 'durasi']);
    var DATA_UNIQ = 1;

    $form.onSubmit(function(data) {
        console.log('submit', data)
        $http.post('/api/admin/reservasi/store', data).then(function(resp) {
            // HelperService.redirect('/admin/reservasi');
        })
    })

    show();
    function show(){
        $('#outlet-content').append(outletBoxContentTemplate(DATA_UNIQ, show_delete_btn = false));
        loadUnitSelect2(DATA_UNIQ);
        DATA_UNIQ++;
    }

    $('#btn-add-outlet').click(function() {
        user_id = $('.user-select').find(':selected').val();
        $('#outlet-content').append(outletBoxContentTemplate(DATA_UNIQ));
        loadUnitSelect2(DATA_UNIQ);
        DATA_UNIQ++;
    })

    $(document).on('click', '.btn-delete-outlet', function() {
        console.log('klik')
        uniq = $(this).attr('data-uniq');
        $('.container-detail-card-' + uniq).remove();
    })

    $('#id_paket').change(function(){
        id = $(this).val();
        $http.get('/api/admin/reservasi/select2-get-include-item/' + id)
            .then(function(resp){
                console.log(resp.data.get_item)
                template = '';
                $.each(resp.data.get_item, function(k, row){
                    template += additionalItemTemplate(DATA_UNIQ, row);  
                    DATA_UNIQ++;           
                })

                $('#additional-item').append(template);
            })
            .catch(function(request, status, error){
                $('#additional-item').html('');
            })     
    })

    function outletBoxContentTemplate(uniq, show_delete_btn = true) {
            btn_delete_style = 'display:none';
            if(show_delete_btn){
                btn_delete_style = '';
            }
            var template = `
            <div class="card container-detail-card-${uniq}" style="margin-bottom: 20px;">
                <input type="hidden" id="get_id_uniq" value="${uniq}">
                <div class="card-body">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Item</label>
                                    <select class="form-control unit-select unit-select-${uniq}" id="unit_id" name="unit_id" style="width:100%" data-uniq="${uniq}">
                                        <option value="">Select Item</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Qty</label>
                                    <input type="number" class="form-control qty" id="qty" name="qty" placeholder="QTY">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Durasi</label>
                                    <input type="number" class="form-control durasi" id="durasi" name="durasi" placeholder="Durasi">
                                </div>
                            </div>
                        </div>
                    </div>
                    <button style="${btn_delete_style}" type="button" class="btn btn-sm btn-danger waves-effect btn-delete-outlet" data-uniq="${uniq}"><i
                            class="fa fa-trash"></i> Delete Item</button>
                </div>
            </div>
        `;
            return template;
    }

    function loadUnitSelect2(uniq) {
        HelperService
            .select2Static(".unit-select-" + uniq, '/admin/reservasi/select2-item', function(item) {
                return {
                    id: item.id,
                    text: item.nama_barang,
                    data: item,
                }
            })
    }

    function additionalItemTemplate(uniq, row) {
            var template = `
            <div class="card container-detail-card-${uniq}" style="margin-bottom: 20px;">
                <input type="hidden" id="get_id_uniq" value="${uniq}">
                <div class="card-body">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Item</label>
                                    <input type="text" class="form-control include_item" name="include_item" placeholder="item" value="${row.get_nama_barang.nama_barang}" disabled>
                                    <input type="hidden" class="form-control include_id" name="include_id" placeholder="ids" value="${row.id_item_pelayanan}" disabled>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Qty</label>
                                    <input type="number" class="form-control include_qty" name="include_qty" placeholder="QTY">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        `;
            return template;
    }

</script>
@endsection