@extends($master_template)

@section('content')
<!-- Default box -->
<div class="card">
    <div class="card-header">
        <h3 class="card-title">{{ $title }}</h3>

        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                <i class="fas fa-times"></i>
            </button>
        </div>
    </div>
    <div class="card-body">
        <button type="button" class="btn btn-primary add-item-vendor-btn"><i class="fa fa-plus"></i> ADD NEW VENDORS ITEM</button>
        <table id="vendor-table"></table>
    </div>
    <!-- /.card-body -->
    <div class="card-footer">
        Footer
    </div>
    <!-- /.card-footer-->
</div>
<!-- /.card -->


<form id="item-vendor-create-form">
    <div id="item-vendor-create-modal" class="modal fade" tabindex="-1" aria-labelledby="item-vendor-create-modalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="item-vendor-create-modalLabel">ADD NEW VENDOR ITEM
                    </h4>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="mb-3">
                        <label>Bussiness</label>
                        <select class="form-control col-12" name="vendor_id" required>
                            <option selected value="">Choose...</option>
                            @foreach($vendor as $item)
                                <option value="{{ $item->id }}">{{ $item->vendor_name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="mb-3">
                        <label>Produk Name</label>
                        <input type="text" name="nama_barang" class="form-control" value="" placeholder="Product Name">
                    </div>
                    <div class="mb-3">
                        <label>Stock</label>
                        <input type="number" name="stok" class="form-control" value="" placeholder="enter Stok" >
                    </div>
                    <div class="mb-3">
                        <label>Selling Price</label>
                        <input type="number" name="harga_jual" class="form-control" value="" placeholder="enter Selling Price" >
                    </div>
                    <div class="mb-3">
                        <label>Finance Price</label>
                        <input type="number" name="pembiayaan" class="form-control" value="" placeholder="enter Finance Price" >
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light" data-bs-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-dark">Save changes</button>
                </div>
            </div>
        </div>
    </div>
</form>

<form id="item-vendor-edit-form">
    <div id="item-vendor-edit-modal" class="modal fade" tabindex="-1" aria-labelledby="item-vendor-edit-modalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="item-vendor-edit-modalLabel">EDIT VENDOR ITEM
                    </h4>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="id" />
                    <div class="mb-3">
                        <label>Bussiness</label>
                        <select class="form-control col-12" name="vendor_id" required>
                            <option selected value="">Choose...</option>
                            @foreach($vendor as $item)
                                <option value="{{ $item->id }}">{{ $item->vendor_name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="mb-3">
                        <label>Produk Name</label>
                        <input type="text" name="nama_barang" class="form-control" value="" placeholder="Product Name">
                    </div>
                    <div class="mb-3">
                        <label>Stock</label>
                        <input type="number" name="stok" class="form-control" value="" placeholder="enter Stok" >
                    </div>
                    <div class="mb-3">
                        <label>Selling Price</label>
                        <input type="number" name="harga_jual" class="form-control" value="" placeholder="enter Selling Price" >
                    </div>
                    <div class="mb-3">
                        <label>Finance Price</label>
                        <input type="number" name="pembiayaan" class="form-control" value="" placeholder="enter Finance Price" >
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light" data-bs-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-dark">Save changes</button>
                </div>
            </div>
        </div>
    </div>
</form>
@endsection

@section('js')
<script>
    $(function() {
        var $table = $('#vendor-table');
        var $modalCreate = $('#item-vendor-create-modal');
        var $modalEdit = $('#item-vendor-edit-modal');

        var $formCreate = new FormService($('#item-vendor-create-form'));
        var $formEdit = new FormService($('#item-vendor-edit-form'));

        var $http = new HttpService();

        var $httpCreate = new HttpService({
            formService: $formCreate,
            bootrapTable: $table,
            bootrapModal: $modalCreate,
        });

        var $httpEdit = new HttpService({
            formService: $formEdit,
            bootrapTable: $table,
            bootrapModal: $modalEdit,
        });

        $table.bootstrapTable({
            sortName: 'nama_barang',
            sortOrder: 'ASC',
            theadClasses: 'table-dark',
            url: HelperService.base_url + '/api/admin/vendor-items',
            columns: [{
                    title: 'NO',
                    sortable: true,
                    formatter: function(value, row, index) {
                        return index + 1;
                    }
                },
                {
                    title: 'Product Name',
                    field: 'id',
                    sortable: true,
                    formatter: function(value, row, index) {
                        return row.nama_barang;
                    }
                },
                {
                    title: 'Stock',
                    field: 'id',
                    sortable: true,
                    formatter: function(value, row, index) {
                        return row.stok;
                    }
                },
                {
                    title: 'Selling Price',
                    field: 'harga_jual',
                    sortable: true,
                },
                {
                    title: 'Finance Price',
                    field: 'pembiayaan',
                    sortable: true,
                },
                {
                    title: 'ACTION',
                    formatter: function(value, row, index) {
                        var link = '';
                        link += '<button type="button" class="btn btn-sm btn-xs btn-success edit">';
                        link += 'Edit';
                        link += '</button>';
                        link += '<button type="button" class="btn btn-sm btn-xs btn-danger delete">';
                        link += 'Delete';
                        link += '</button>';
                        return link;
                    },
                    events: {
                        'click .edit': function(e, value, row) {
                            $formEdit.emptyFormData();
                            $modalEdit.modal('show')
                            $formEdit.setFormData({
                                id: row.id,
                                nama_barang: row.nama_barang,
                                stok: row.stok,
                                harga_jual: row.harga_jual,
                                pembiayaan: row.pembiayaan,
                                vendor_id: row.vendor_id,
                                // vendor_id: row.vendor_id,
                            });
                        },
                        'click .delete': function(e, value, row) {
                            $http.delete(`/api/admin/vendor-items/${row.id}/delete`)
                                .then(function(resp) {
                                    $table.bootstrapTable('refresh');
                                })
                        }
                    }
                },
            ]
        });

        $('.add-item-vendor-btn').click(function() {
            $formCreate.emptyFormData();
            $modalCreate.modal('show')
        });

        $formCreate.onSubmit(function(data) {
            $httpCreate.post('/api/admin/vendor-items', data);
        });

        $formEdit.onSubmit(function(data) {
            $httpEdit.put(`/api/admin/vendor-items/${data.id}/update`, data)
        });
    })
</script>
@endsection